﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework_lesson_1_task_3_rectangle
{
    public partial class Form1 : Form
    {
        bool _ctrl;

        public Form1()
        {
            _ctrl = false;
            Text = "Rectangle";
            InitializeComponent();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {            
            switch (e.Button)
            { 
                case MouseButtons.Left:
                    if (_ctrl) this.Close();
                    if (e.X < 10 || e.X > ClientSize.Width - 10 || e.Y < 10 || e.Y > ClientSize.Height - 10)
                    {
                        MessageBox.Show("Outside!", "Coordinates", MessageBoxButtons.OK);
                    }
                    else
                        if (e.X == 10 || e.X == ClientSize.Width - 10 || e.Y == 10 || e.Y == ClientSize.Height - 10)
                        {
                            MessageBox.Show("On the border!", "Coordinates", MessageBoxButtons.OK);
                        }
                        else MessageBox.Show("Inside!", "Coordinates", MessageBoxButtons.OK);
                    break;
                case MouseButtons.Right:
                    Text = string.Format("Width: {0}   Height: {1}", ClientSize.Width, ClientSize.Height);
                    break;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                _ctrl = true;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                _ctrl = false;
            }
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            Text = string.Format("x = {0}   y = {1}", e.X, e.Y);
        }       
    }
}
