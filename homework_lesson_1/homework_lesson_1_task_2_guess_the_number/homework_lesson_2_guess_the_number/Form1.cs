﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework_lesson_2_guess_the_number
{
    public partial class Form1 : Form
    {
        int min;
        int max;
        int count;
        Random rnd;
        DialogResult res;
        int rndNumber;
        bool exit;
        
        public Form1()
        {
            exit = false;
            count = 0;
            rnd = new Random();
            InitializeComponent();
            MessageBox.Show("Think of a number from 0 to 2000 and press enter key", "Guess the number", MessageBoxButtons.OK, MessageBoxIcon.Information);
            InitMinMax();
            do
            {
                try
                {
                    Guess();
                }
                catch (Exception e)
                {
                    if (MessageBox.Show(e.Message + " Wanna play again?", "Guess the number", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        exit = true;
                    }
                    else
                    {
                        MessageBox.Show("Think of a number from 0 to 2000 and press enter key", "Guess the number", MessageBoxButtons.OK, MessageBoxIcon.Question);
                        InitMinMax();
                        count = 0;
                    }
                }
            }
            while (!exit);
        }

        void InitMinMax()
        {
            min = 0;
            max = 2000; 
        }
        
        void Guess()
        {           
            count++;
            if (min > max)
            {
                throw new DataException("Your entered wrong data!");
            }
            rndNumber = rnd.Next(min, max + 1);                
            string str = string.Format("Your number is {0}", rndNumber);
            res = MessageBox.Show(str, "Guess the number", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(res == DialogResult.No)
            {
                res = MessageBox.Show("Your number is biger?", "Guess the number", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(res == DialogResult.Yes)
                {
                    min = rndNumber + 1;
                }
                else
                {
                    max = rndNumber - 1;
                }
            }
            else
            {
                res = MessageBox.Show("I'm guessing in " + count + " attempts, try again?", "Guess the number", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.No) exit = true;
                else
                {
                    MessageBox.Show("Think of a number from 0 to 2000 and press enter key", "Guess the number", MessageBoxButtons.OK, MessageBoxIcon.Question);
                    count = 0;
                    InitMinMax();                        
                }
            }            
        }
    }
}
