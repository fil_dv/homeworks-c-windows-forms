﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace homework_lesson_1_resume
{
    public partial class Form1 : Form
    {
        int _count;
        public Form1()
        {
            InitializeComponent();
            _count = 0;
            ShowMessages();
        }

        void ShowMessages()
        {
            string[] arrStr = new string[3] { "My name is Dmitry", "My surname is Filonenko", "I am a student" };
            for(int i = 0; i < arrStr.Length; ++i)
            {
                _count += arrStr[i].Length;
                MessageBox.Show(arrStr[i], i == (arrStr.Length - 1) ? "average = " + (_count / arrStr.Length).ToString() : "resume", MessageBoxButtons.OK); 
            }
        }
    }
}
