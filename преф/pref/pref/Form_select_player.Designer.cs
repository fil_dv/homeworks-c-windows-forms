﻿namespace pref
{
    partial class Form_select_players
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_select_players));
            this.label1 = new System.Windows.Forms.Label();
            this.button_3_player = new System.Windows.Forms.Button();
            this.button_4_player = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_pulya = new System.Windows.Forms.NumericUpDown();
            this.textBox_player_1 = new System.Windows.Forms.TextBox();
            this.textBox_player_2 = new System.Windows.Forms.TextBox();
            this.textBox_player_3 = new System.Windows.Forms.TextBox();
            this.textBox_player_4 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button_3_load = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button_4_load = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pulya)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(39, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Введите имена игроков:";
            // 
            // button_3_player
            // 
            this.button_3_player.Enabled = false;
            this.button_3_player.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_3_player.Location = new System.Drawing.Point(43, 281);
            this.button_3_player.Name = "button_3_player";
            this.button_3_player.Size = new System.Drawing.Size(95, 35);
            this.button_3_player.TabIndex = 1;
            this.button_3_player.Text = "3 игрока";
            this.button_3_player.UseVisualStyleBackColor = true;
            this.button_3_player.Click += new System.EventHandler(this.button_3_player_Click);
            // 
            // button_4_player
            // 
            this.button_4_player.Enabled = false;
            this.button_4_player.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_4_player.Location = new System.Drawing.Point(178, 281);
            this.button_4_player.Name = "button_4_player";
            this.button_4_player.Size = new System.Drawing.Size(93, 35);
            this.button_4_player.TabIndex = 2;
            this.button_4_player.Text = "4 игрока";
            this.button_4_player.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(92, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Пуля:";
            // 
            // numericUpDown_pulya
            // 
            this.numericUpDown_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.numericUpDown_pulya.Location = new System.Drawing.Point(167, 50);
            this.numericUpDown_pulya.Name = "numericUpDown_pulya";
            this.numericUpDown_pulya.Size = new System.Drawing.Size(47, 26);
            this.numericUpDown_pulya.TabIndex = 4;
            this.numericUpDown_pulya.Value = new decimal(new int[] {
            11,
            0,
            0,
            0});
            // 
            // textBox_player_1
            // 
            this.textBox_player_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_player_1.Location = new System.Drawing.Point(93, 134);
            this.textBox_player_1.Name = "textBox_player_1";
            this.textBox_player_1.Size = new System.Drawing.Size(178, 26);
            this.textBox_player_1.TabIndex = 5;
            this.textBox_player_1.TextChanged += new System.EventHandler(this.textBox_player_1_TextChanged);
            // 
            // textBox_player_2
            // 
            this.textBox_player_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_player_2.Location = new System.Drawing.Point(93, 166);
            this.textBox_player_2.Name = "textBox_player_2";
            this.textBox_player_2.Size = new System.Drawing.Size(178, 26);
            this.textBox_player_2.TabIndex = 6;
            this.textBox_player_2.TextChanged += new System.EventHandler(this.textBox_player_2_TextChanged);
            // 
            // textBox_player_3
            // 
            this.textBox_player_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_player_3.Location = new System.Drawing.Point(93, 198);
            this.textBox_player_3.Name = "textBox_player_3";
            this.textBox_player_3.Size = new System.Drawing.Size(178, 26);
            this.textBox_player_3.TabIndex = 7;
            this.textBox_player_3.TextChanged += new System.EventHandler(this.textBox_player_3_TextChanged);
            // 
            // textBox_player_4
            // 
            this.textBox_player_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox_player_4.Location = new System.Drawing.Point(93, 230);
            this.textBox_player_4.Name = "textBox_player_4";
            this.textBox_player_4.Size = new System.Drawing.Size(178, 26);
            this.textBox_player_4.TabIndex = 8;
            this.textBox_player_4.TextChanged += new System.EventHandler(this.textBox_player_4_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(27, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "Игрок 1:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(27, 172);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Игрок 2:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(27, 204);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 16);
            this.label6.TabIndex = 12;
            this.label6.Text = "Игрок 3:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(27, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 13;
            this.label7.Text = "Игрок 4:";
            // 
            // button_3_load
            // 
            this.button_3_load.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_3_load.Location = new System.Drawing.Point(43, 399);
            this.button_3_load.Name = "button_3_load";
            this.button_3_load.Size = new System.Drawing.Size(95, 33);
            this.button_3_load.TabIndex = 14;
            this.button_3_load.Text = "3 игрока";
            this.button_3_load.UseVisualStyleBackColor = true;
            this.button_3_load.Click += new System.EventHandler(this.button_load_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 339);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(277, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "_____________________________________________";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(39, 368);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(228, 20);
            this.label8.TabIndex = 16;
            this.label8.Text = "Загрузить сохраненную игру:";
            // 
            // button_4_load
            // 
            this.button_4_load.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_4_load.Location = new System.Drawing.Point(176, 399);
            this.button_4_load.Name = "button_4_load";
            this.button_4_load.Size = new System.Drawing.Size(95, 33);
            this.button_4_load.TabIndex = 17;
            this.button_4_load.Text = "4 игрока";
            this.button_4_load.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(73, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(165, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "Создать новую игру:";
            // 
            // Form_select_players
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(308, 448);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.button_4_load);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_3_load);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_player_4);
            this.Controls.Add(this.textBox_player_3);
            this.Controls.Add(this.textBox_player_2);
            this.Controls.Add(this.textBox_player_1);
            this.Controls.Add(this.numericUpDown_pulya);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button_4_player);
            this.Controls.Add(this.button_3_player);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_select_players";
            this.Text = "Установки игры";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_pulya)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_3_player;
        private System.Windows.Forms.Button button_4_player;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.NumericUpDown numericUpDown_pulya;
        public System.Windows.Forms.TextBox textBox_player_1;
        public System.Windows.Forms.TextBox textBox_player_2;
        public System.Windows.Forms.TextBox textBox_player_3;
        public System.Windows.Forms.TextBox textBox_player_4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button_3_load;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_4_load;
        private System.Windows.Forms.Label label9;
    }
}

