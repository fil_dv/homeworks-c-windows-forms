﻿namespace pref
{
    partial class Form_3_players
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_3_players));
            this.label_pulya = new System.Windows.Forms.Label();
            this.label_player_1_name = new System.Windows.Forms.Label();
            this.label_player_2_name = new System.Windows.Forms.Label();
            this.label_player_3_name = new System.Windows.Forms.Label();
            this.points_player_1 = new System.Windows.Forms.Label();
            this.points_player_2 = new System.Windows.Forms.Label();
            this.points_player_3 = new System.Windows.Forms.Label();
            this.player_1_pulya = new System.Windows.Forms.TextBox();
            this.player_1_add_pulya = new System.Windows.Forms.TextBox();
            this.player_3_add_pulya = new System.Windows.Forms.TextBox();
            this.player_2_add_pulya = new System.Windows.Forms.TextBox();
            this.player_1_add_gora = new System.Windows.Forms.TextBox();
            this.player_2_add_gora = new System.Windows.Forms.TextBox();
            this.player_3_add_gora = new System.Windows.Forms.TextBox();
            this.add_vist_1_2 = new System.Windows.Forms.TextBox();
            this.add_vist_1_3 = new System.Windows.Forms.TextBox();
            this.add_vist_2_3 = new System.Windows.Forms.TextBox();
            this.add_vist_2_1 = new System.Windows.Forms.TextBox();
            this.add_vist_3_1 = new System.Windows.Forms.TextBox();
            this.add_vist_3_2 = new System.Windows.Forms.TextBox();
            this.button_submit = new System.Windows.Forms.Button();
            this.label_vist_1_2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label_vist_1_3 = new System.Windows.Forms.Label();
            this.label_vist_2_1 = new System.Windows.Forms.Label();
            this.label_vist_2_3 = new System.Windows.Forms.Label();
            this.label_vist_3_1 = new System.Windows.Forms.Label();
            this.label_vist_3_2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.player_2_pulya = new System.Windows.Forms.TextBox();
            this.player_3_pulya = new System.Windows.Forms.TextBox();
            this.button_prew = new System.Windows.Forms.Button();
            this.button_next = new System.Windows.Forms.Button();
            this.level_counter = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.файлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.новаяТграToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитьИгруToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.загрузитьИгруToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.player_1_gora = new System.Windows.Forms.TextBox();
            this.player_2_gora = new System.Windows.Forms.TextBox();
            this.player_3_gora = new System.Windows.Forms.TextBox();
            this.vist_1_2 = new System.Windows.Forms.TextBox();
            this.vist_2_1 = new System.Windows.Forms.TextBox();
            this.vist_1_3 = new System.Windows.Forms.TextBox();
            this.vist_2_3 = new System.Windows.Forms.TextBox();
            this.vist_3_1 = new System.Windows.Forms.TextBox();
            this.vist_3_2 = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_pulya
            // 
            this.label_pulya.AutoSize = true;
            this.label_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_pulya.Location = new System.Drawing.Point(497, 272);
            this.label_pulya.Name = "label_pulya";
            this.label_pulya.Size = new System.Drawing.Size(29, 20);
            this.label_pulya.TabIndex = 0;
            this.label_pulya.Text = "25";
            this.label_pulya.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_player_1_name
            // 
            this.label_player_1_name.AutoSize = true;
            this.label_player_1_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_player_1_name.Location = new System.Drawing.Point(333, 80);
            this.label_player_1_name.Name = "label_player_1_name";
            this.label_player_1_name.Size = new System.Drawing.Size(73, 20);
            this.label_player_1_name.TabIndex = 1;
            this.label_player_1_name.Text = "Игрок 1";
            // 
            // label_player_2_name
            // 
            this.label_player_2_name.AutoSize = true;
            this.label_player_2_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_player_2_name.Location = new System.Drawing.Point(545, 80);
            this.label_player_2_name.Name = "label_player_2_name";
            this.label_player_2_name.Size = new System.Drawing.Size(73, 20);
            this.label_player_2_name.TabIndex = 2;
            this.label_player_2_name.Text = "Игрок 2";
            // 
            // label_player_3_name
            // 
            this.label_player_3_name.AutoSize = true;
            this.label_player_3_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_player_3_name.Location = new System.Drawing.Point(455, 347);
            this.label_player_3_name.Name = "label_player_3_name";
            this.label_player_3_name.Size = new System.Drawing.Size(73, 20);
            this.label_player_3_name.TabIndex = 3;
            this.label_player_3_name.Text = "Игрок 3";
            // 
            // points_player_1
            // 
            this.points_player_1.AutoSize = true;
            this.points_player_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.points_player_1.Location = new System.Drawing.Point(453, 76);
            this.points_player_1.Name = "points_player_1";
            this.points_player_1.Size = new System.Drawing.Size(21, 24);
            this.points_player_1.TabIndex = 4;
            this.points_player_1.Text = "0";
            // 
            // points_player_2
            // 
            this.points_player_2.AutoSize = true;
            this.points_player_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.points_player_2.Location = new System.Drawing.Point(662, 76);
            this.points_player_2.Name = "points_player_2";
            this.points_player_2.Size = new System.Drawing.Size(21, 24);
            this.points_player_2.TabIndex = 5;
            this.points_player_2.Text = "0";
            // 
            // points_player_3
            // 
            this.points_player_3.AutoSize = true;
            this.points_player_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.points_player_3.Location = new System.Drawing.Point(533, 344);
            this.points_player_3.Name = "points_player_3";
            this.points_player_3.Size = new System.Drawing.Size(21, 24);
            this.points_player_3.TabIndex = 6;
            this.points_player_3.Text = "0";
            // 
            // player_1_pulya
            // 
            this.player_1_pulya.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.player_1_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_1_pulya.Location = new System.Drawing.Point(270, 84);
            this.player_1_pulya.Multiline = true;
            this.player_1_pulya.Name = "player_1_pulya";
            this.player_1_pulya.ReadOnly = true;
            this.player_1_pulya.Size = new System.Drawing.Size(25, 322);
            this.player_1_pulya.TabIndex = 7;
            // 
            // player_1_add_pulya
            // 
            this.player_1_add_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_1_add_pulya.Location = new System.Drawing.Point(50, 260);
            this.player_1_add_pulya.Name = "player_1_add_pulya";
            this.player_1_add_pulya.Size = new System.Drawing.Size(50, 26);
            this.player_1_add_pulya.TabIndex = 8;
            this.player_1_add_pulya.TextChanged += new System.EventHandler(this.player_1_add_pulya_TextChanged);
            // 
            // player_3_add_pulya
            // 
            this.player_3_add_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_3_add_pulya.Location = new System.Drawing.Point(435, 610);
            this.player_3_add_pulya.Name = "player_3_add_pulya";
            this.player_3_add_pulya.Size = new System.Drawing.Size(50, 26);
            this.player_3_add_pulya.TabIndex = 12;
            this.player_3_add_pulya.TextChanged += new System.EventHandler(this.player_2_add_pulya_TextChanged);
            // 
            // player_2_add_pulya
            // 
            this.player_2_add_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_2_add_pulya.Location = new System.Drawing.Point(906, 260);
            this.player_2_add_pulya.Name = "player_2_add_pulya";
            this.player_2_add_pulya.Size = new System.Drawing.Size(50, 26);
            this.player_2_add_pulya.TabIndex = 13;
            this.player_2_add_pulya.TextChanged += new System.EventHandler(this.player_3_ad_pulya_TextChanged);
            // 
            // player_1_add_gora
            // 
            this.player_1_add_gora.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_1_add_gora.Location = new System.Drawing.Point(50, 321);
            this.player_1_add_gora.Name = "player_1_add_gora";
            this.player_1_add_gora.Size = new System.Drawing.Size(50, 26);
            this.player_1_add_gora.TabIndex = 14;
            this.player_1_add_gora.TextChanged += new System.EventHandler(this.player_1_add_gora_TextChanged);
            // 
            // player_2_add_gora
            // 
            this.player_2_add_gora.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_2_add_gora.Location = new System.Drawing.Point(906, 321);
            this.player_2_add_gora.Name = "player_2_add_gora";
            this.player_2_add_gora.Size = new System.Drawing.Size(50, 26);
            this.player_2_add_gora.TabIndex = 15;
            this.player_2_add_gora.TextChanged += new System.EventHandler(this.player_2_add_gora_TextChanged);
            // 
            // player_3_add_gora
            // 
            this.player_3_add_gora.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_3_add_gora.Location = new System.Drawing.Point(535, 610);
            this.player_3_add_gora.Name = "player_3_add_gora";
            this.player_3_add_gora.Size = new System.Drawing.Size(50, 26);
            this.player_3_add_gora.TabIndex = 16;
            this.player_3_add_gora.TextChanged += new System.EventHandler(this.player_3_add_gora_TextChanged);
            // 
            // add_vist_1_2
            // 
            this.add_vist_1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_1_2.Location = new System.Drawing.Point(50, 142);
            this.add_vist_1_2.Name = "add_vist_1_2";
            this.add_vist_1_2.Size = new System.Drawing.Size(50, 26);
            this.add_vist_1_2.TabIndex = 17;
            this.add_vist_1_2.TextChanged += new System.EventHandler(this.vist_1_2_TextChanged);
            // 
            // add_vist_1_3
            // 
            this.add_vist_1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_1_3.Location = new System.Drawing.Point(50, 476);
            this.add_vist_1_3.Name = "add_vist_1_3";
            this.add_vist_1_3.Size = new System.Drawing.Size(50, 26);
            this.add_vist_1_3.TabIndex = 18;
            this.add_vist_1_3.TextChanged += new System.EventHandler(this.vist_1_3_TextChanged);
            // 
            // add_vist_2_3
            // 
            this.add_vist_2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_2_3.Location = new System.Drawing.Point(906, 476);
            this.add_vist_2_3.Name = "add_vist_2_3";
            this.add_vist_2_3.Size = new System.Drawing.Size(50, 26);
            this.add_vist_2_3.TabIndex = 19;
            this.add_vist_2_3.TextChanged += new System.EventHandler(this.vist_2_3_TextChanged);
            // 
            // add_vist_2_1
            // 
            this.add_vist_2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_2_1.Location = new System.Drawing.Point(906, 142);
            this.add_vist_2_1.Name = "add_vist_2_1";
            this.add_vist_2_1.Size = new System.Drawing.Size(50, 26);
            this.add_vist_2_1.TabIndex = 20;
            this.add_vist_2_1.TextChanged += new System.EventHandler(this.vist_2_1_TextChanged);
            // 
            // add_vist_3_1
            // 
            this.add_vist_3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_3_1.Location = new System.Drawing.Point(259, 610);
            this.add_vist_3_1.Name = "add_vist_3_1";
            this.add_vist_3_1.Size = new System.Drawing.Size(50, 26);
            this.add_vist_3_1.TabIndex = 21;
            this.add_vist_3_1.TextChanged += new System.EventHandler(this.vist_3_1_TextChanged);
            // 
            // add_vist_3_2
            // 
            this.add_vist_3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.add_vist_3_2.Location = new System.Drawing.Point(707, 610);
            this.add_vist_3_2.Name = "add_vist_3_2";
            this.add_vist_3_2.Size = new System.Drawing.Size(50, 26);
            this.add_vist_3_2.TabIndex = 22;
            this.add_vist_3_2.TextChanged += new System.EventHandler(this.vist_3_2_TextChanged);
            // 
            // button_submit
            // 
            this.button_submit.Enabled = false;
            this.button_submit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button_submit.Location = new System.Drawing.Point(884, 603);
            this.button_submit.Name = "button_submit";
            this.button_submit.Size = new System.Drawing.Size(101, 33);
            this.button_submit.TabIndex = 23;
            this.button_submit.Text = "Записать";
            this.button_submit.UseVisualStyleBackColor = true;
            this.button_submit.Click += new System.EventHandler(this.button_submit_Click);
            // 
            // label_vist_1_2
            // 
            this.label_vist_1_2.AutoSize = true;
            this.label_vist_1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_1_2.Location = new System.Drawing.Point(47, 115);
            this.label_vist_1_2.Name = "label_vist_1_2";
            this.label_vist_1_2.Size = new System.Drawing.Size(35, 16);
            this.label_vist_1_2.TabIndex = 24;
            this.label_vist_1_2.Text = "         ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(22, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Висты на игрока";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(22, 436);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Висты на игрока";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(881, 436);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.TabIndex = 27;
            this.label3.Text = "Висты на игрока";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(881, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 16);
            this.label4.TabIndex = 28;
            this.label4.Text = "Висты на игрока";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(210, 591);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 16);
            this.label5.TabIndex = 29;
            this.label5.Text = "Висты на игрока ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(656, 591);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 16);
            this.label6.TabIndex = 30;
            this.label6.Text = "Висты на игрока ";
            // 
            // label_vist_1_3
            // 
            this.label_vist_1_3.AutoSize = true;
            this.label_vist_1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_1_3.Location = new System.Drawing.Point(47, 452);
            this.label_vist_1_3.Name = "label_vist_1_3";
            this.label_vist_1_3.Size = new System.Drawing.Size(35, 16);
            this.label_vist_1_3.TabIndex = 31;
            this.label_vist_1_3.Text = "         ";
            // 
            // label_vist_2_1
            // 
            this.label_vist_2_1.AutoSize = true;
            this.label_vist_2_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_2_1.Location = new System.Drawing.Point(903, 115);
            this.label_vist_2_1.Name = "label_vist_2_1";
            this.label_vist_2_1.Size = new System.Drawing.Size(35, 16);
            this.label_vist_2_1.TabIndex = 32;
            this.label_vist_2_1.Text = "         ";
            // 
            // label_vist_2_3
            // 
            this.label_vist_2_3.AutoSize = true;
            this.label_vist_2_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_2_3.Location = new System.Drawing.Point(903, 452);
            this.label_vist_2_3.Name = "label_vist_2_3";
            this.label_vist_2_3.Size = new System.Drawing.Size(35, 16);
            this.label_vist_2_3.TabIndex = 33;
            this.label_vist_2_3.Text = "         ";
            // 
            // label_vist_3_1
            // 
            this.label_vist_3_1.AutoSize = true;
            this.label_vist_3_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_3_1.Location = new System.Drawing.Point(320, 591);
            this.label_vist_3_1.Name = "label_vist_3_1";
            this.label_vist_3_1.Size = new System.Drawing.Size(35, 16);
            this.label_vist_3_1.TabIndex = 34;
            this.label_vist_3_1.Text = "         ";
            // 
            // label_vist_3_2
            // 
            this.label_vist_3_2.AutoSize = true;
            this.label_vist_3_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_vist_3_2.Location = new System.Drawing.Point(768, 591);
            this.label_vist_3_2.Name = "label_vist_3_2";
            this.label_vist_3_2.Size = new System.Drawing.Size(35, 16);
            this.label_vist_3_2.TabIndex = 35;
            this.label_vist_3_2.Text = "         ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(50, 241);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 16);
            this.label7.TabIndex = 36;
            this.label7.Text = " Пуля :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(906, 241);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 16);
            this.label8.TabIndex = 37;
            this.label8.Text = " Пуля :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(432, 591);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 16);
            this.label9.TabIndex = 38;
            this.label9.Text = " Пуля :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(50, 302);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 39;
            this.label10.Text = " Гора :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(908, 302);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(48, 16);
            this.label11.TabIndex = 40;
            this.label11.Text = " Гора :";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(537, 591);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 16);
            this.label12.TabIndex = 41;
            this.label12.Text = " Гора :";
            // 
            // player_2_pulya
            // 
            this.player_2_pulya.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.player_2_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_2_pulya.Location = new System.Drawing.Point(733, 80);
            this.player_2_pulya.Multiline = true;
            this.player_2_pulya.Name = "player_2_pulya";
            this.player_2_pulya.ReadOnly = true;
            this.player_2_pulya.Size = new System.Drawing.Size(25, 322);
            this.player_2_pulya.TabIndex = 42;
            // 
            // player_3_pulya
            // 
            this.player_3_pulya.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.player_3_pulya.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.player_3_pulya.Location = new System.Drawing.Point(361, 468);
            this.player_3_pulya.Name = "player_3_pulya";
            this.player_3_pulya.ReadOnly = true;
            this.player_3_pulya.Size = new System.Drawing.Size(322, 17);
            this.player_3_pulya.TabIndex = 43;
            // 
            // button_prew
            // 
            this.button_prew.Enabled = false;
            this.button_prew.Location = new System.Drawing.Point(351, 1);
            this.button_prew.Name = "button_prew";
            this.button_prew.Size = new System.Drawing.Size(75, 23);
            this.button_prew.TabIndex = 44;
            this.button_prew.Text = "<<";
            this.button_prew.UseVisualStyleBackColor = true;
            this.button_prew.Click += new System.EventHandler(this.button_prew_Click);
            // 
            // button_next
            // 
            this.button_next.Enabled = false;
            this.button_next.Location = new System.Drawing.Point(608, 1);
            this.button_next.Name = "button_next";
            this.button_next.Size = new System.Drawing.Size(75, 23);
            this.button_next.TabIndex = 45;
            this.button_next.Text = ">>";
            this.button_next.UseVisualStyleBackColor = true;
            this.button_next.Click += new System.EventHandler(this.button_next_Click);
            // 
            // level_counter
            // 
            this.level_counter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.level_counter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.level_counter.Location = new System.Drawing.Point(549, 5);
            this.level_counter.Name = "level_counter";
            this.level_counter.ReadOnly = true;
            this.level_counter.Size = new System.Drawing.Size(26, 19);
            this.level_counter.TabIndex = 47;
            this.level_counter.Text = "00";
            this.level_counter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(459, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(93, 19);
            this.textBox1.TabIndex = 48;
            this.textBox1.Text = "Раздача №";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.файлToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 49;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // файлToolStripMenuItem
            // 
            this.файлToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.новаяТграToolStripMenuItem,
            this.сохранитьИгруToolStripMenuItem,
            this.загрузитьИгруToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.файлToolStripMenuItem.Name = "файлToolStripMenuItem";
            this.файлToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.файлToolStripMenuItem.Text = "Файл";
            // 
            // новаяТграToolStripMenuItem
            // 
            this.новаяТграToolStripMenuItem.Name = "новаяТграToolStripMenuItem";
            this.новаяТграToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.новаяТграToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.новаяТграToolStripMenuItem.Text = "Новая игра";
            this.новаяТграToolStripMenuItem.Click += new System.EventHandler(this.NewGameToolStripMenuItem_Click);
            // 
            // сохранитьИгруToolStripMenuItem
            // 
            this.сохранитьИгруToolStripMenuItem.Name = "сохранитьИгруToolStripMenuItem";
            this.сохранитьИгруToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьИгруToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.сохранитьИгруToolStripMenuItem.Text = "Сохранить игру";
            this.сохранитьИгруToolStripMenuItem.Click += new System.EventHandler(this.SaveGameToolStripMenuItem_Click);
            // 
            // загрузитьИгруToolStripMenuItem
            // 
            this.загрузитьИгруToolStripMenuItem.Name = "загрузитьИгруToolStripMenuItem";
            this.загрузитьИгруToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.загрузитьИгруToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.загрузитьИгруToolStripMenuItem.Text = "Загрузить игру";
            this.загрузитьИгруToolStripMenuItem.Click += new System.EventHandler(this.OpenGameToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.справкаToolStripMenuItem.Text = "Справка";
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе...";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // player_1_gora
            // 
            this.player_1_gora.Location = new System.Drawing.Point(371, 126);
            this.player_1_gora.Multiline = true;
            this.player_1_gora.Name = "player_1_gora";
            this.player_1_gora.ReadOnly = true;
            this.player_1_gora.Size = new System.Drawing.Size(75, 166);
            this.player_1_gora.TabIndex = 51;
            // 
            // player_2_gora
            // 
            this.player_2_gora.Location = new System.Drawing.Point(593, 126);
            this.player_2_gora.Multiline = true;
            this.player_2_gora.Name = "player_2_gora";
            this.player_2_gora.ReadOnly = true;
            this.player_2_gora.Size = new System.Drawing.Size(71, 166);
            this.player_2_gora.TabIndex = 52;
            // 
            // player_3_gora
            // 
            this.player_3_gora.Location = new System.Drawing.Point(402, 387);
            this.player_3_gora.Multiline = true;
            this.player_3_gora.Name = "player_3_gora";
            this.player_3_gora.ReadOnly = true;
            this.player_3_gora.Size = new System.Drawing.Size(228, 65);
            this.player_3_gora.TabIndex = 53;
            // 
            // vist_1_2
            // 
            this.vist_1_2.Location = new System.Drawing.Point(184, 83);
            this.vist_1_2.Multiline = true;
            this.vist_1_2.Name = "vist_1_2";
            this.vist_1_2.ReadOnly = true;
            this.vist_1_2.Size = new System.Drawing.Size(66, 166);
            this.vist_1_2.TabIndex = 54;
            // 
            // vist_2_1
            // 
            this.vist_2_1.Location = new System.Drawing.Point(780, 84);
            this.vist_2_1.Multiline = true;
            this.vist_2_1.Name = "vist_2_1";
            this.vist_2_1.ReadOnly = true;
            this.vist_2_1.Size = new System.Drawing.Size(66, 166);
            this.vist_2_1.TabIndex = 55;
            // 
            // vist_1_3
            // 
            this.vist_1_3.Location = new System.Drawing.Point(184, 328);
            this.vist_1_3.Multiline = true;
            this.vist_1_3.Name = "vist_1_3";
            this.vist_1_3.ReadOnly = true;
            this.vist_1_3.Size = new System.Drawing.Size(66, 166);
            this.vist_1_3.TabIndex = 56;
            // 
            // vist_2_3
            // 
            this.vist_2_3.Location = new System.Drawing.Point(780, 328);
            this.vist_2_3.Multiline = true;
            this.vist_2_3.Name = "vist_2_3";
            this.vist_2_3.ReadOnly = true;
            this.vist_2_3.Size = new System.Drawing.Size(66, 166);
            this.vist_2_3.TabIndex = 57;
            // 
            // vist_3_1
            // 
            this.vist_3_1.Location = new System.Drawing.Point(299, 499);
            this.vist_3_1.Multiline = true;
            this.vist_3_1.Name = "vist_3_1";
            this.vist_3_1.ReadOnly = true;
            this.vist_3_1.Size = new System.Drawing.Size(166, 66);
            this.vist_3_1.TabIndex = 58;
            // 
            // vist_3_2
            // 
            this.vist_3_2.Location = new System.Drawing.Point(564, 499);
            this.vist_3_2.Multiline = true;
            this.vist_3_2.Name = "vist_3_2";
            this.vist_3_2.ReadOnly = true;
            this.vist_3_2.Size = new System.Drawing.Size(166, 66);
            this.vist_3_2.TabIndex = 59;
            // 
            // Form_3_players
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1008, 655);
            this.Controls.Add(this.vist_3_2);
            this.Controls.Add(this.vist_3_1);
            this.Controls.Add(this.vist_2_3);
            this.Controls.Add(this.vist_1_3);
            this.Controls.Add(this.vist_2_1);
            this.Controls.Add(this.vist_1_2);
            this.Controls.Add(this.player_3_gora);
            this.Controls.Add(this.player_2_gora);
            this.Controls.Add(this.player_1_gora);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.level_counter);
            this.Controls.Add(this.button_next);
            this.Controls.Add(this.button_prew);
            this.Controls.Add(this.player_3_pulya);
            this.Controls.Add(this.player_2_pulya);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label_vist_3_2);
            this.Controls.Add(this.label_vist_3_1);
            this.Controls.Add(this.label_vist_2_3);
            this.Controls.Add(this.label_vist_2_1);
            this.Controls.Add(this.label_vist_1_3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label_vist_1_2);
            this.Controls.Add(this.button_submit);
            this.Controls.Add(this.add_vist_3_2);
            this.Controls.Add(this.add_vist_3_1);
            this.Controls.Add(this.add_vist_2_1);
            this.Controls.Add(this.add_vist_2_3);
            this.Controls.Add(this.add_vist_1_3);
            this.Controls.Add(this.add_vist_1_2);
            this.Controls.Add(this.player_3_add_gora);
            this.Controls.Add(this.player_2_add_gora);
            this.Controls.Add(this.player_1_add_gora);
            this.Controls.Add(this.player_2_add_pulya);
            this.Controls.Add(this.player_3_add_pulya);
            this.Controls.Add(this.player_1_add_pulya);
            this.Controls.Add(this.player_1_pulya);
            this.Controls.Add(this.points_player_3);
            this.Controls.Add(this.points_player_2);
            this.Controls.Add(this.points_player_1);
            this.Controls.Add(this.label_player_3_name);
            this.Controls.Add(this.label_player_2_name);
            this.Controls.Add(this.label_player_1_name);
            this.Controls.Add(this.label_pulya);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Form_3_players";
            this.Text = "Пуля";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_3_players_FormClosing);
            this.Load += new System.EventHandler(this.Form_3_players_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label_pulya;
        public System.Windows.Forms.Label label_player_1_name;
        public System.Windows.Forms.Label label_player_2_name;
        public System.Windows.Forms.Label label_player_3_name;
        public System.Windows.Forms.Label points_player_1;
        public System.Windows.Forms.Label points_player_2;
        public System.Windows.Forms.Label points_player_3;
        private System.Windows.Forms.TextBox player_1_pulya;
        private System.Windows.Forms.TextBox player_1_add_pulya;
        private System.Windows.Forms.TextBox player_3_add_pulya;
        private System.Windows.Forms.TextBox player_2_add_pulya;
        private System.Windows.Forms.TextBox player_1_add_gora;
        private System.Windows.Forms.TextBox player_2_add_gora;
        private System.Windows.Forms.TextBox player_3_add_gora;
        private System.Windows.Forms.TextBox add_vist_1_2;
        private System.Windows.Forms.TextBox add_vist_1_3;
        private System.Windows.Forms.TextBox add_vist_2_3;
        private System.Windows.Forms.TextBox add_vist_2_1;
        private System.Windows.Forms.TextBox add_vist_3_1;
        private System.Windows.Forms.TextBox add_vist_3_2;
        private System.Windows.Forms.Button button_submit;
        private System.Windows.Forms.Label label_vist_1_2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label_vist_1_3;
        private System.Windows.Forms.Label label_vist_2_1;
        private System.Windows.Forms.Label label_vist_2_3;
        private System.Windows.Forms.Label label_vist_3_1;
        private System.Windows.Forms.Label label_vist_3_2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox player_2_pulya;
        private System.Windows.Forms.TextBox player_3_pulya;
        public System.Windows.Forms.Button button_prew;
        public System.Windows.Forms.Button button_next;
        public System.Windows.Forms.TextBox level_counter;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem файлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитьИгруToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem загрузитьИгруToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.TextBox player_1_gora;
        private System.Windows.Forms.TextBox player_2_gora;
        private System.Windows.Forms.TextBox player_3_gora;
        private System.Windows.Forms.TextBox vist_1_2;
        private System.Windows.Forms.TextBox vist_2_1;
        private System.Windows.Forms.TextBox vist_1_3;
        private System.Windows.Forms.TextBox vist_2_3;
        private System.Windows.Forms.TextBox vist_3_1;
        private System.Windows.Forms.TextBox vist_3_2;
        private System.Windows.Forms.ToolStripMenuItem новаяТграToolStripMenuItem;
    }
}