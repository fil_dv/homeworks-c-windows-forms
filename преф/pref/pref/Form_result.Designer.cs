﻿namespace pref
{
    partial class Form_result
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_result));
            this.button_result = new System.Windows.Forms.Button();
            this.res_label_player_1 = new System.Windows.Forms.Label();
            this.res_label_player_2 = new System.Windows.Forms.Label();
            this.res_label_player_3 = new System.Windows.Forms.Label();
            this.res_label_player_4 = new System.Windows.Forms.Label();
            this.res_label_points_1 = new System.Windows.Forms.Label();
            this.res_label_points_4 = new System.Windows.Forms.Label();
            this.res_label_points_3 = new System.Windows.Forms.Label();
            this.res_label_points_2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_result
            // 
            this.button_result.Location = new System.Drawing.Point(98, 219);
            this.button_result.Name = "button_result";
            this.button_result.Size = new System.Drawing.Size(75, 23);
            this.button_result.TabIndex = 0;
            this.button_result.Text = "Ok";
            this.button_result.UseVisualStyleBackColor = true;
            // 
            // res_label_player_1
            // 
            this.res_label_player_1.AutoSize = true;
            this.res_label_player_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_player_1.Location = new System.Drawing.Point(38, 44);
            this.res_label_player_1.Name = "res_label_player_1";
            this.res_label_player_1.Size = new System.Drawing.Size(78, 20);
            this.res_label_player_1.TabIndex = 1;
            this.res_label_player_1.Text = "Player_1";
            // 
            // res_label_player_2
            // 
            this.res_label_player_2.AutoSize = true;
            this.res_label_player_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_player_2.Location = new System.Drawing.Point(38, 83);
            this.res_label_player_2.Name = "res_label_player_2";
            this.res_label_player_2.Size = new System.Drawing.Size(78, 20);
            this.res_label_player_2.TabIndex = 2;
            this.res_label_player_2.Text = "Player_2";
            // 
            // res_label_player_3
            // 
            this.res_label_player_3.AutoSize = true;
            this.res_label_player_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_player_3.Location = new System.Drawing.Point(38, 123);
            this.res_label_player_3.Name = "res_label_player_3";
            this.res_label_player_3.Size = new System.Drawing.Size(78, 20);
            this.res_label_player_3.TabIndex = 3;
            this.res_label_player_3.Text = "Player_3";
            // 
            // res_label_player_4
            // 
            this.res_label_player_4.AutoSize = true;
            this.res_label_player_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_player_4.Location = new System.Drawing.Point(38, 164);
            this.res_label_player_4.Name = "res_label_player_4";
            this.res_label_player_4.Size = new System.Drawing.Size(78, 20);
            this.res_label_player_4.TabIndex = 4;
            this.res_label_player_4.Text = "Player_4";
            this.res_label_player_4.Visible = false;
            // 
            // res_label_points_1
            // 
            this.res_label_points_1.AutoSize = true;
            this.res_label_points_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_points_1.Location = new System.Drawing.Point(189, 44);
            this.res_label_points_1.Name = "res_label_points_1";
            this.res_label_points_1.Size = new System.Drawing.Size(29, 20);
            this.res_label_points_1.TabIndex = 5;
            this.res_label_points_1.Text = "00";
            // 
            // res_label_points_4
            // 
            this.res_label_points_4.AutoSize = true;
            this.res_label_points_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_points_4.Location = new System.Drawing.Point(189, 164);
            this.res_label_points_4.Name = "res_label_points_4";
            this.res_label_points_4.Size = new System.Drawing.Size(29, 20);
            this.res_label_points_4.TabIndex = 6;
            this.res_label_points_4.Text = "00";
            // 
            // res_label_points_3
            // 
            this.res_label_points_3.AutoSize = true;
            this.res_label_points_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_points_3.Location = new System.Drawing.Point(189, 123);
            this.res_label_points_3.Name = "res_label_points_3";
            this.res_label_points_3.Size = new System.Drawing.Size(29, 20);
            this.res_label_points_3.TabIndex = 7;
            this.res_label_points_3.Text = "00";
            // 
            // res_label_points_2
            // 
            this.res_label_points_2.AutoSize = true;
            this.res_label_points_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.res_label_points_2.Location = new System.Drawing.Point(189, 83);
            this.res_label_points_2.Name = "res_label_points_2";
            this.res_label_points_2.Size = new System.Drawing.Size(29, 20);
            this.res_label_points_2.TabIndex = 8;
            this.res_label_points_2.Text = "00";
            // 
            // Form_result
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(266, 258);
            this.Controls.Add(this.res_label_points_2);
            this.Controls.Add(this.res_label_points_3);
            this.Controls.Add(this.res_label_points_4);
            this.Controls.Add(this.res_label_points_1);
            this.Controls.Add(this.res_label_player_4);
            this.Controls.Add(this.res_label_player_3);
            this.Controls.Add(this.res_label_player_2);
            this.Controls.Add(this.res_label_player_1);
            this.Controls.Add(this.button_result);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form_result";
            this.Text = "Результаты игры";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_result;
        private System.Windows.Forms.Label res_label_player_1;
        private System.Windows.Forms.Label res_label_player_2;
        private System.Windows.Forms.Label res_label_player_3;
        private System.Windows.Forms.Label res_label_player_4;
        private System.Windows.Forms.Label res_label_points_1;
        private System.Windows.Forms.Label res_label_points_4;
        private System.Windows.Forms.Label res_label_points_3;
        private System.Windows.Forms.Label res_label_points_2;
    }
}