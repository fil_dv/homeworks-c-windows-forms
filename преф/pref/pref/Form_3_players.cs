﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace pref
{
    public partial class Form_3_players : Form
    {
        Form_select_players _parent;
        public Form_select_players Parent3P { get { return _parent; } }
        public Manager _calc;
        public List<TextBox> _addList;
        public List<TextBox> _recordsList;
        public List<Label> _pointsList;
        bool _isSave;

        public Form_3_players(Form_select_players parent)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            _parent = parent;
            _addList = new List<TextBox>();
            _recordsList = new List<TextBox>();
            _pointsList = new List<Label>();
            CreateAddList();
            CreateRecordsList();
            CreatePointsList();
            _calc = new Manager(this);
            _isSave = true;
        }

        private void Form_3_players_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Close("Текущая игра не сохранена. Уверены, что хотите закрыть её?"))
            {
                _parent.Close();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void Form_3_players_Load(object sender, EventArgs e)
        {
            button_submit.Enabled = false;
            SetData();
            button_submit.Focus();
        }

        void SetData()
        {
            label_pulya.Text = Convert.ToString(_parent.numericUpDown_pulya.Value);
            label_player_1_name.Text = _parent.textBox_player_1.Text + ':';
            label_player_2_name.Text = _parent.textBox_player_2.Text + ':';
            label_player_3_name.Text = _parent.textBox_player_3.Text + ':';
            label_vist_1_2.Text = "\"" + _parent.textBox_player_2.Text + "\":";
            label_vist_1_3.Text = "\"" + _parent.textBox_player_3.Text + "\":";
            label_vist_2_1.Text = "\"" + _parent.textBox_player_1.Text + "\":";
            label_vist_2_3.Text = "\"" + _parent.textBox_player_3.Text + "\":";
            label_vist_3_1.Text = "\"" + _parent.textBox_player_1.Text + "\":";
            label_vist_3_2.Text = "\"" + _parent.textBox_player_2.Text + "\":";
        }

        public void SetNames(string name_1, string name_2, string name_3)
        {
            label_player_1_name.Text = name_1 + ':';
            label_player_2_name.Text = name_2 + ':';
            label_player_3_name.Text = name_3 + ':';
            label_vist_1_2.Text = "\"" + name_2 + "\":";
            label_vist_1_3.Text = "\"" + name_3 + "\":";
            label_vist_2_1.Text = "\"" + name_1 + "\":";
            label_vist_2_3.Text = "\"" + name_3 + "\":";
            label_vist_3_1.Text = "\"" + name_1 + "\":";
            label_vist_3_2.Text = "\"" + name_2 + "\":";
        }

        void CreateAddList()
        {
            _addList.Add(player_1_add_pulya);
            _addList.Add(player_1_add_gora);
            _addList.Add(add_vist_1_2);
            _addList.Add(add_vist_1_3);
            _addList.Add(player_2_add_pulya);
            _addList.Add(player_2_add_gora);
            _addList.Add(add_vist_2_1);
            _addList.Add(add_vist_2_3);
            _addList.Add(player_3_add_pulya);
            _addList.Add(player_3_add_gora);
            _addList.Add(add_vist_3_1);
            _addList.Add(add_vist_3_2);
        }

        void CreateRecordsList()
        {
            _recordsList.Add(player_1_pulya);
            _recordsList.Add(player_1_gora);
            _recordsList.Add(vist_1_2);
            _recordsList.Add(vist_1_3);
            _recordsList.Add(player_2_pulya);
            _recordsList.Add(player_2_gora);
            _recordsList.Add(vist_2_1);
            _recordsList.Add(vist_2_3);
            _recordsList.Add(player_3_pulya);
            _recordsList.Add(player_3_gora);
            _recordsList.Add(vist_3_1);
            _recordsList.Add(vist_3_2);
        }

        void CreatePointsList()
        {
            _pointsList.Add(points_player_1);
            _pointsList.Add(points_player_2);
            _pointsList.Add(points_player_3);
        }
        
        private void vist_1_2_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_1_2);
        }

        private void player_1_add_pulya_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_1_add_pulya);
        }

        private void player_1_add_gora_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_1_add_gora);
        }

        private void vist_1_3_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_1_3);
        }

        private void vist_2_1_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_2_1);
        }

        private void player_3_ad_pulya_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_2_add_pulya);
        }

        private void player_2_add_gora_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_2_add_gora);
        }

        private void vist_2_3_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_2_3);
        }

        private void vist_3_1_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_3_1);
        }

        private void player_2_add_pulya_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_3_add_pulya);
        }

        private void player_3_add_gora_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(player_3_add_gora);
        }

        private void vist_3_2_TextChanged(object sender, EventArgs e)
        {
            EnablingSubmitButton();
            ChekInputData(add_vist_3_2);
        }

        void EnablingSubmitButton()
        {
            if (player_1_add_gora.Text.Length == 0 && player_2_add_gora.Text.Length == 0 && player_3_add_gora.Text.Length == 0 &&
                player_1_add_pulya.Text.Length == 0 && player_3_add_pulya.Text.Length == 0 && player_2_add_pulya.Text.Length == 0 &&
                add_vist_1_2.Text.Length == 0 && add_vist_1_3.Text.Length == 0 &&
                add_vist_2_1.Text.Length == 0 && add_vist_2_3.Text.Length == 0 &&
                add_vist_3_1.Text.Length == 0 && add_vist_3_2.Text.Length == 0)
            {
                button_submit.Enabled = false;
            }
            else
            {
                button_submit.Enabled = true;
            }
        }

        void ChekInputData(TextBox tb)
        {
            string actualdata = string.Empty;
            char[] entereddata = tb.Text.ToCharArray();
            foreach (char aChar in entereddata.AsEnumerable())
            {
                if (Char.IsDigit(aChar))
                {
                    actualdata = actualdata + aChar;
                }
                else
                {
                    actualdata.Replace(aChar, ' ');
                    actualdata.Trim();
                }
            }
            tb.Text = actualdata;
        }

        private void button_submit_Click(object sender, EventArgs e)
        {
            _isSave = false;
            _calc.Record();
           if (_calc.GameNumber > 0) button_prew.Enabled = true;
           else button_prew.Enabled = false;
        }

        private void button_prew_Click(object sender, EventArgs e)
        {
            _calc.Prew();
        }

        private void button_next_Click(object sender, EventArgs e)
        {
            _calc.Next();
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        private void SaveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _isSave = true;
            _calc.SaveFile();
        }

        private void OpenGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Close("Текущая игра не сохранена. Уверены, что хотите загрузить другую?"))
            {
                _calc.OpenFile();
            }
            else
            {
                return;
            }
            
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Close("Текущая игра не сохранена. Уверены, что хотите закрыть её?"))
            {
                _parent.Close();
            }
            else
            {
                return;
            }
        }

        bool Close(string str)
        {
            if (!_isSave)
            {
                DialogResult dr = MessageBox.Show(str, "Пуля", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;            
        }

        private void NewGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Close("Текущая игра не сохранена. Уверены, что хотите создать новую?"))
            {                
                _parent.textBox_player_1.Text = "";
                _parent.textBox_player_2.Text = "";
                _parent.textBox_player_3.Text = "";
                _parent.numericUpDown_pulya.Value = 11;
                _parent.Visible = true;
                                     
            }
            else
            {
                return;
            }            
        }

        public void NewGame()
        {
            SetData();
            _calc.ResetData();    
        }
       
    }
}
