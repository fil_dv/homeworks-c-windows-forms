﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace pref
{
    public class Manager
    {
        Form_3_players _form_3;
        int _countOfPlayers;
        List<int> _currentValues;          // список интов для хранения текущих значений пуль, гор, вистов
        List<int> _currentValuesCopy;
        List<int> _prewValues;
        List<string> _currentText;  // список текущих значений 
        List<string> _prewText;  // список предыдущих значений (для просмотра истории записей)
        List<string> _playersNames;
        List<string> _resText;
        List<int> _res;
        List<int> _prewRes;
        int _gameCounter;  // счетчик раздач
        public int GameNumber { get { return _gameCounter; } }
        //Form_4_players _form_4;
        
        public Manager(Form_3_players form_3)
        {
            _countOfPlayers = 3;
            _gameCounter = 0;
            _form_3 = form_3;            
            _currentText = new List<string>();
            _prewText = new List<string>();
            _currentValues = new List<int>();
            _currentValuesCopy = new List<int>();
            _prewValues = new List<int>();
            _playersNames = new List<string>();
            _resText = new List<string>();
            _res = new List<int>();
            _prewRes = new List<int>();

            InitLists(_form_3._recordsList.Count);
            InitResults(_countOfPlayers);
        }

       // public Calculator(Form_4_players form_4)
        //{
            //_form_4 = form_4;
            //_addList = form_4.addList;
            //_recordsList = form_4.recordsList;
        //}

        void InitLists(int count)
        {           
            for (int i = 0; i < count; ++i)
            {
                _currentValues.Add(0);
                _currentValuesCopy.Add(0);
                _prewValues.Add(0);
                _prewText.Add(string.Empty);
                _currentText.Add(string.Empty);                
            }             
        }

        void InitResults(int count)
        {
            for (int i = 0; i < count; ++i)
            {
                _res.Add(0);
                _prewRes.Add(0);
                _resText.Add(string.Empty);
            }
        }

        void SetNames()
        {
            _playersNames.Add(_form_3.Parent3P.textBox_player_1.Text);
            _playersNames.Add(_form_3.Parent3P.textBox_player_2.Text);
            _playersNames.Add(_form_3.Parent3P.textBox_player_3.Text);
        }

        public void Record()
        {
            InitPrew();
            _form_3.button_next.Enabled = false;
            _form_3.level_counter.Text = _gameCounter >= 9? Convert.ToString(++_gameCounter) : "0" + Convert.ToString(++_gameCounter);

            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                if (_form_3._addList[i].Text.Length > 0)
                {
                    //if(i == )
                    
                    //AddValues();
                    _currentValues[i] += Convert.ToInt32(_form_3._addList[i].Text);

                    if (_form_3._recordsList[i].Text.Length == 0) _form_3._recordsList[i].Text += Convert.ToString(_currentValues[i]);
                    else
                    {
                        _form_3._recordsList[i].Text += ("," +  Convert.ToString(_currentValues[i]));                       
                    } 
                }                
            }
            CurrentCopy();            
            EmtingAddFields();
            Calulator();
            for (int i = 0; i < _countOfPlayers; ++i)
            {
                _form_3._pointsList[i].Text = Convert.ToString(_res[i]);
            }
        }

        void AddValues()
        {
            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                if (_form_3._addList[i].Text.Length > 0)
                {
                    AddValues();
                    _currentValues[i] += Convert.ToInt32(_form_3._addList[i].Text);

                    if (_form_3._recordsList[i].Text.Length == 0) _form_3._recordsList[i].Text += Convert.ToString(_currentValues[i]);
                    else
                    {
                        _form_3._recordsList[i].Text += ("," + Convert.ToString(_currentValues[i]));
                    }
                }
            }
        }
        
        void Calulator()
        {
            int gora_1 = _currentValues[1];
            int gora_2 = _currentValues[5];
            int gora_3 = _currentValues[9];

            int vist_1_2 = _currentValues[2];
            int vist_1_3 = _currentValues[3];
            int vist_2_1 = _currentValues[6];
            int vist_2_3 = _currentValues[7];
            int vist_3_1 = _currentValues[10];
            int vist_3_2 = _currentValues[11];

            _res[0] = (gora_1 - gora_2) * (-10) + (gora_1 - gora_3) * (-10) + (vist_1_2 - vist_2_1) + (vist_1_3 - vist_3_1);
            _res[1] = (gora_2 - gora_1) * (-10) + (gora_2 - gora_3) * (-10) + (vist_2_1 - vist_1_2) + (vist_2_3 - vist_3_2);
            _res[2] = (gora_3 - gora_1) * (-10) + (gora_3 - gora_2) * (-10) + (vist_3_1 - vist_1_3) + (vist_3_2 - vist_2_3);
        }
      
        void InitPrew()
        {
            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                _prewText[i] = _currentText[i];
                _prewValues[i] = _currentValues[i];
            }
            for (int i = 0; i < _countOfPlayers; ++i)
            {
                _prewRes[i] = _res[i];
            }
        }

        void CurrentCopy()
        {
            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                _currentText[i] = _form_3._recordsList[i].Text;
                _currentValuesCopy[i] = _currentValues[i];
            }
        }       

        void EmtingAddFields()
        {
            foreach (TextBox tb in _form_3._addList)
            {
                tb.Text = string.Empty; 
            }
        }

        public void Prew()
        {
            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                _currentValues[i] = _prewValues[i];
                _form_3._recordsList[i].Text = _prewText[i];
            }
            for (int i = 0; i < _countOfPlayers; ++i)
            {
                _form_3._pointsList[i].Text = Convert.ToString(_prewRes[i]);
            }

            _form_3.level_counter.Text =_gameCounter > 10? Convert.ToString(--_gameCounter) : "0" + Convert.ToString(--_gameCounter);
            _form_3.button_prew.Enabled = false;
            _form_3.button_next.Enabled = true;
        }

        public void Next()
        {
            for (int i = 0; i < _form_3._recordsList.Count; ++i)
            {
                _form_3._recordsList[i].Text = _currentText[i];
                _currentValues[i] = _currentValuesCopy[i];
            }
            for (int i = 0; i < _countOfPlayers; ++i)
            {
                _form_3._pointsList[i].Text = Convert.ToString(_res[i]);
            }
            _form_3.level_counter.Text = _gameCounter >= 9 ? Convert.ToString(++_gameCounter) : "0" + Convert.ToString(++_gameCounter);
            _form_3.button_prew.Enabled = true;
            _form_3.button_next.Enabled = false;
        }

        public void SaveFile()
        {
            using (SaveFileDialog saveFD = new SaveFileDialog())
            {
                saveFD.Filter = "Text files(*.txt)|*.txt";
                saveFD.InitialDirectory = @"D:\"; ////////////////////////////////////////////////////////////////////////
                saveFD.CheckFileExists = false;
                DateTime thisDay = DateTime.Today;
                saveFD.FileName = "пуля " + thisDay.ToString("d");
                saveFD.DefaultExt = ".txt";
                if (saveFD.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream fs = new FileStream(saveFD.FileName, FileMode.Create))
                    {
                        using (StreamWriter sw = new StreamWriter(fs))
                        {
                            string str = "";
                            sw.Write(CreateStringForSaveFile(str));
                        }
                    }
                }
            }
        }

        string CreateStringForSaveFile(string str)
        {
            foreach (TextBox tb in _form_3._recordsList)
            {
                if (tb.Text.Length == 0)
                {
                    str += ("0/");
                }
                else
                {
                    str += (tb.Text + "/");
                }
            }
            str = str.Remove(str.Length - 1);
            str += "*";
            foreach (int i in _currentValues)
            {
                str += (Convert.ToString(i) + "/");
            }
            str = str.Remove(str.Length - 1);
            str += "*";
            str += Convert.ToString(_gameCounter);
            str += "*";
            str += _form_3.label_pulya.Text;
            str += "*";
            SetNames();
            foreach (string s in _playersNames)
            {
                str += (s + "/");
            }
            str = str.Remove(str.Length - 1);
            str += "*";
            foreach (Label lb in _form_3._pointsList)
            {
                if (lb.Text.Length == 0)
                {
                    str += ("0/");
                }
                else
                {
                    str += (lb.Text + "/");
                }
            }
            str = str.Remove(str.Length - 1);
            return str;
        }

        public void OpenFile()
        { 
            using (OpenFileDialog openFD = new OpenFileDialog())
            {
                openFD.Filter = "Text files(*.txt)|*.txt";
                openFD.CheckFileExists = true;
                openFD.InitialDirectory = @"D:\";   //////////////////////////////////////////////////////
                if (openFD.ShowDialog() == DialogResult.OK)
                {
                    using (FileStream fs = new FileStream(openFD.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (StreamReader sr = new StreamReader(fs))
                        {
                            string str = sr.ReadToEnd();
                            InitListsFromString(str);
                        }
                    }
                }
            }
        }

        void InitListsFromString(string str)
        {
            string[] strArr = str.Split('*');
            string[] arrText = strArr[0].Split('/');
            string[] arrVal = strArr[1].Split('/');
            _gameCounter = Convert.ToInt32(strArr[2]);
            _form_3.label_pulya.Text = strArr[3];
            _form_3.level_counter.Text = Convert.ToString(_gameCounter);
            string[] arrName = strArr[4].Split('/');
            string[] arrPoints = strArr[5].Split('/');
            for (int i = 0; i < arrText.Length; ++i)
            {
                if (arrText[i] == "0") _form_3._recordsList[i].Text = "";
                else _form_3._recordsList[i].Text = arrText[i];
                _currentText[i] = _form_3._recordsList[i].Text;
                _currentValues[i] = Convert.ToInt32(arrVal[i]);
                _currentValuesCopy[i] = _currentValues[i];
                _prewValues[i] = _currentValues[i];
            }
            _form_3.SetNames(arrName[0], arrName[1], arrName[2]);

            for (int i = 0; i < _countOfPlayers; ++i )
            {
                _form_3._pointsList[i].Text = arrPoints[i];
                _res[i] = Convert.ToInt32(arrPoints[i]);
            }
        }

        public void ResetData()
        {
            for (int i = 0; i < _currentValues.Count; ++i)
            {
                _currentValues[i] = 0;
                _currentValuesCopy[i] = 0;
                _prewValues[i] = 0;
                _prewText[i] = "";
                _currentText[i] = "";
                _form_3._recordsList[i].Text = "";
            }
            _gameCounter = 0;
            _form_3.level_counter.Text = "00";
        }


    }
}
