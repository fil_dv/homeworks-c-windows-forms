﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pref
{
    public partial class Form_select_players : Form
    {
        Form_3_players f3p;
        bool _isFirst;
        
        public Form_select_players()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            f3p = new Form_3_players(this);
            _isFirst = true;

            textBox_player_1.Text = "Димон";
            textBox_player_2.Text = "Паха";
            textBox_player_3.Text = "Ларин";
        }

        private void button_3_player_Click(object sender, EventArgs e)
        {            
            if (_isFirst)
            {                
                f3p.Show();
                _isFirst = false;
            }
            else 
            {
                f3p.NewGame();
            }
            this.Hide();
        }

        private void textBox_player_1_TextChanged(object sender, EventArgs e)
        {
            EnablingButtons();
        }

        private void textBox_player_2_TextChanged(object sender, EventArgs e)
        {
            EnablingButtons();
        }

        private void textBox_player_3_TextChanged(object sender, EventArgs e)
        {
            EnablingButtons();
        }

        private void textBox_player_4_TextChanged(object sender, EventArgs e)
        {
            EnablingButtons();
        }

        void EnablingButtons()
        {
            if (textBox_player_1.Text.Length > 0 && textBox_player_2.Text.Length > 0 && textBox_player_3.Text.Length > 0 && textBox_player_4.Text.Length == 0)
            {
                button_3_player.Enabled = true;
                button_4_player.Enabled = false;
            }
            else
                if (textBox_player_1.Text.Length > 0 && textBox_player_2.Text.Length > 0 && textBox_player_3.Text.Length > 0 && textBox_player_4.Text.Length > 0)
                {
                    button_3_player.Enabled = false;
                    button_4_player.Enabled = true;
                }
                else
                {
                    button_3_player.Enabled = false;
                    button_4_player.Enabled = false;
                }
        }

        private void button_load_Click(object sender, EventArgs e)
        {
            button_3_player_Click(sender, e);
            f3p._calc.OpenFile();
        }
    }
}
