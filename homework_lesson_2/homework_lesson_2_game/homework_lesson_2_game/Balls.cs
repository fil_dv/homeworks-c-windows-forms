﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace homework_lesson_2_game
{
    class Balls
    {
        List<Label> _ballsList;
        public List<Label> BallsList { get { return _ballsList; } }

        public Balls()
        {
            _ballsList = new List<Label>();
        }

        public void Add(int xMin, int xMax, int y)
        {           
            Label lb = new Label();
            Random rnd = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            int x = rnd.Next(xMin, xMax);
            lb.Location = new Point(x, y);            
            lb.BorderStyle = BorderStyle.Fixed3D;
            lb.BackColor = Color.Black;
            lb.BringToFront();
            lb.Width = 20;
            lb.Height = 20;
            lb.Enabled = false;
            _ballsList.Add(lb);
        }

        public void RemoveFirstBall()
        {
            _ballsList.RemoveAt(0);
        }

        public int Caught(int batXStart, int batXFinish, int batY, int shift)
        {
            if (_ballsList[0].Bounds.Y <= batY && _ballsList[0].Bounds.Y + shift > batY)
            {
                if (_ballsList[0].Bounds.X >= batXStart && _ballsList[0].Bounds.X <= batXFinish)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
            else return 0;
        }


        public void MoveBalls(int shift)
        {
            foreach (Label lb in _ballsList)
            {
                lb.Location = new Point(lb.Bounds.X, lb.Bounds.Y + shift);
            }
        }



    }
}
