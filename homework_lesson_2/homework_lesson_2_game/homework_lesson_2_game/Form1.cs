﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace homework_lesson_2_game
{
    public partial class Form1 : Form
    {      
        int _xMin, _xMax, _y, _batX, _batY, _startBat, _finishBat, _count, _shift;
        //bool _left, _right;
        KeyStates left;
        KeyStates right;
        Balls _balls;
        
        public Form1()
        {
            try
            {
                InitializeComponent();
               // _left = false;
               // _right = false;
                timer_game.Interval = 10;
                _startBat = panel1.Bounds.X - 13;
                _finishBat = panel1.ClientSize.Width - label_bat.ClientSize.Width;
                _batX = _startBat;
                _shift = 10;
                _y = panel1.Bounds.Y;
                _count = 0;
                _balls = new Balls();
                _xMin = panel1.Bounds.X;
                _xMax = panel1.ClientSize.Width - 4;
                button_start.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                BatMover();     // двигаем "ракетку"
                if (_count == 150)
                {
                    _balls.Add(_xMin, _xMax, _y);   // вбрасываем "мячи"
                }
                _count++;
                if (_count == 151) _count = 0;
                _balls.MoveBalls((int)numericUpDown_level.Value);   // двигаем "мячи"
                if (_balls.BallsList.Count != 0)
                {
                    foreach (Label lb in _balls.BallsList)  // отрисовываем "мячи" на форме
                    {
                        this.Controls.Add(lb);
                        lb.BringToFront();
                    }
                }
                if (_balls.BallsList.Count != 0)
                {
                    Keep();     // "ловим мячи"               
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Keep()
        {
            try
            {
                switch (_balls.Caught(label_bat.Bounds.X, label_bat.Bounds.X + label_bat.ClientSize.Width, label_bat.Bounds.Y, (int)numericUpDown_level.Value)) 
                {
                    case 1: // если "поймал"
                        this.Controls.Remove(_balls.BallsList[0]);
                        _balls.RemoveFirstBall(); 
                        progressBar_ok.Value++;
                        if (progressBar_ok.Value == progressBar_ok.Maximum)
                        {
                            timer_game.Stop();
                            DialogResult dr = MessageBox.Show("Start next level?", "level completed!", MessageBoxButtons.YesNo);
                            if (dr == DialogResult.Yes)
                            {
                                Clear((int)++numericUpDown_level.Value);
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    break;
                    case -1:    // если не "поймал"
                        this.Controls.Remove(_balls.BallsList[0]);
                        _balls.RemoveFirstBall(); 
                        progressBar_lost.Value++;
                        if (progressBar_lost.Value == progressBar_lost.Maximum)
                        {
                            timer_game.Stop();
                            DialogResult dr = MessageBox.Show("game over!", "Play again?", MessageBoxButtons.YesNo);
                            if (dr == DialogResult.Yes)
                            {
                                progressBar_lost.Value = 0;
                                Clear();
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    break;
                    default:
                    break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
        void BatMover()
        {            
            try
            {
               /* this.Focus();
                if (_left)
                {
                    _batX -= _shift;
                }
                if (_right)
                {
                    _batX += _shift;
                }
                _batY = label_bat.Bounds.Y;
                // label_bat.Focus();////////////////////////////////
                label_bat.Location = new Point(_batX, _batY);
                */
                
                
                
                left = Keyboard.GetKeyStates(Key.Left);
                right = Keyboard.GetKeyStates(Key.Right);
            
                if (left == KeyStates.Down && _batX > _startBat)
                {
                    _batX -= _shift;
                }
                if (right == KeyStates.Down && _batX < _finishBat)
                {
                    _batX += _shift;
                }
                _batY = label_bat.Bounds.Y;
               // label_bat.Focus();////////////////////////////////
                label_bat.Location = new Point(_batX, _batY);
                 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void Clear(int level = 1)
        {
            try
            {
                foreach (Label lb in _balls.BallsList)  // убираем "мячи" с формы
                {
                    this.Controls.Remove(lb);
                }
                _balls = new Balls();
                numericUpDown_level.Value = level;
                progressBar_ok.Value = progressBar_ok.Minimum;
                button_start.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void button_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_start_Click(object sender, EventArgs e)
        {            
            timer_game.Start();          
        }

        private void button_pause_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            timer_game.Stop();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            /*switch (e.KeyChar)
            {
                case 'n':
                    _left = true;
                    _right = false;
                    break;
                case 'm':
                    _right = true;
                    _left = false;
                    break;               
            }          
            */
        }

        private void Form1_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
           /* switch (e.KeyCode)
            {
                case Keys.Left:
                    _left = true;
                    _right = false;
                    break;
                case Keys.Right:
                    _right = true;
                    _left = false;
                    break;               
            }*/          
        }    

       
    }
}
