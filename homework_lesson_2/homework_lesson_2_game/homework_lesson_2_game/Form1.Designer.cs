﻿namespace homework_lesson_2_game
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_bat = new System.Windows.Forms.Label();
            this.timer_game = new System.Windows.Forms.Timer(this.components);
            this.numericUpDown_level = new System.Windows.Forms.NumericUpDown();
            this.label_level = new System.Windows.Forms.Label();
            this.label_fail = new System.Windows.Forms.Label();
            this.progressBar_lost = new System.Windows.Forms.ProgressBar();
            this.button_start = new System.Windows.Forms.Button();
            this.button_exit = new System.Windows.Forms.Button();
            this.progressBar_ok = new System.Windows.Forms.ProgressBar();
            this.button_pause = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_level)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label_bat);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(357, 256);
            this.panel1.TabIndex = 0;
            // 
            // label_bat
            // 
            this.label_bat.AutoSize = true;
            this.label_bat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label_bat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_bat.Location = new System.Drawing.Point(-2, 237);
            this.label_bat.Name = "label_bat";
            this.label_bat.Size = new System.Drawing.Size(69, 15);
            this.label_bat.TabIndex = 0;
            this.label_bat.Text = "__________";
            // 
            // timer_game
            // 
            this.timer_game.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // numericUpDown_level
            // 
            this.numericUpDown_level.CausesValidation = false;
            this.numericUpDown_level.Location = new System.Drawing.Point(394, 27);
            this.numericUpDown_level.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_level.Name = "numericUpDown_level";
            this.numericUpDown_level.Size = new System.Drawing.Size(55, 20);
            this.numericUpDown_level.TabIndex = 1;
            this.numericUpDown_level.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label_level
            // 
            this.label_level.AutoSize = true;
            this.label_level.Location = new System.Drawing.Point(402, 11);
            this.label_level.Name = "label_level";
            this.label_level.Size = new System.Drawing.Size(32, 13);
            this.label_level.TabIndex = 2;
            this.label_level.Text = "level:";
            // 
            // label_fail
            // 
            this.label_fail.AutoSize = true;
            this.label_fail.Location = new System.Drawing.Point(402, 196);
            this.label_fail.Name = "label_fail";
            this.label_fail.Size = new System.Drawing.Size(47, 13);
            this.label_fail.TabIndex = 3;
            this.label_fail.Text = "lost lifes:";
            // 
            // progressBar_lost
            // 
            this.progressBar_lost.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.progressBar_lost.Location = new System.Drawing.Point(389, 212);
            this.progressBar_lost.Maximum = 3;
            this.progressBar_lost.Name = "progressBar_lost";
            this.progressBar_lost.Size = new System.Drawing.Size(75, 23);
            this.progressBar_lost.Step = 1;
            this.progressBar_lost.TabIndex = 4;
            // 
            // button_start
            // 
            this.button_start.Location = new System.Drawing.Point(389, 70);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(75, 23);
            this.button_start.TabIndex = 5;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(389, 273);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(75, 23);
            this.button_exit.TabIndex = 6;
            this.button_exit.Text = "Exit";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // progressBar_ok
            // 
            this.progressBar_ok.Location = new System.Drawing.Point(12, 274);
            this.progressBar_ok.Maximum = 10;
            this.progressBar_ok.Name = "progressBar_ok";
            this.progressBar_ok.Size = new System.Drawing.Size(357, 23);
            this.progressBar_ok.Step = 1;
            this.progressBar_ok.TabIndex = 7;
            // 
            // button_pause
            // 
            this.button_pause.Location = new System.Drawing.Point(389, 112);
            this.button_pause.Name = "button_pause";
            this.button_pause.Size = new System.Drawing.Size(75, 23);
            this.button_pause.TabIndex = 8;
            this.button_pause.Text = "Pause";
            this.button_pause.UseVisualStyleBackColor = true;
            this.button_pause.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button_pause_MouseClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 303);
            this.Controls.Add(this.button_pause);
            this.Controls.Add(this.progressBar_ok);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.progressBar_lost);
            this.Controls.Add(this.label_fail);
            this.Controls.Add(this.label_level);
            this.Controls.Add(this.numericUpDown_level);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Keeper";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_level)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer_game;
        private System.Windows.Forms.NumericUpDown numericUpDown_level;
        private System.Windows.Forms.Label label_level;
        private System.Windows.Forms.Label label_fail;
        private System.Windows.Forms.ProgressBar progressBar_lost;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.ProgressBar progressBar_ok;
        private System.Windows.Forms.Label label_bat;
        private System.Windows.Forms.Button button_pause;
    }
}

