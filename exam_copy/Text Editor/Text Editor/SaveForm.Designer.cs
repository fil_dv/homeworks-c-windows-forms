﻿namespace Text_Editor
{
    partial class SaveForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button_save = new System.Windows.Forms.Button();
            this.button_dont_save = new System.Windows.Forms.Button();
            this.button_save_cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(245, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Save document`s changes?";
            // 
            // button_save
            // 
            this.button_save.Location = new System.Drawing.Point(63, 70);
            this.button_save.Name = "button_save";
            this.button_save.Size = new System.Drawing.Size(75, 23);
            this.button_save.TabIndex = 1;
            this.button_save.Text = "Save";
            this.button_save.UseVisualStyleBackColor = true;
            this.button_save.Click += new System.EventHandler(this.button_save_Click);
            // 
            // button_dont_save
            // 
            this.button_dont_save.Location = new System.Drawing.Point(156, 70);
            this.button_dont_save.Name = "button_dont_save";
            this.button_dont_save.Size = new System.Drawing.Size(75, 23);
            this.button_dont_save.TabIndex = 2;
            this.button_dont_save.Text = "Don`t save";
            this.button_dont_save.UseVisualStyleBackColor = true;
            this.button_dont_save.Click += new System.EventHandler(this.button_dont_save_Click);
            // 
            // button_save_cancel
            // 
            this.button_save_cancel.Location = new System.Drawing.Point(250, 70);
            this.button_save_cancel.Name = "button_save_cancel";
            this.button_save_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_save_cancel.TabIndex = 3;
            this.button_save_cancel.Text = "Cancel";
            this.button_save_cancel.UseVisualStyleBackColor = true;
            this.button_save_cancel.Click += new System.EventHandler(this.button_cancel_Click);
            // 
            // SaveForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 115);
            this.Controls.Add(this.button_save_cancel);
            this.Controls.Add(this.button_dont_save);
            this.Controls.Add(this.button_save);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SaveForm";
            this.ShowIcon = false;
            this.Text = "SaveForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_save;
        private System.Windows.Forms.Button button_dont_save;
        private System.Windows.Forms.Button button_save_cancel;
    }
}