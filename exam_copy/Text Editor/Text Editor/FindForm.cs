﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Text_Editor
{
    public partial class FindForm : Form
    {
        RichTextBox _textBox;
        string _strForFind;
        int _startIndex;

        public FindForm(RichTextBox textBox)
        {
            InitializeComponent();
            _textBox = textBox;
            _strForFind = "";
            _startIndex = 0;
        }

        private void button_find_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox_for_find_TextChanged(object sender, EventArgs e)
        {
            if (textBox_for_find.Text != "")
            {
                button_find_next.Enabled = true;
                _strForFind = textBox_for_find.Text;
            }
            else
            {
                button_find_next.Enabled = false;
                _strForFind = "";
            }
        }        

        private void button_find_next_Click(object sender, EventArgs e)
        {
            int indexInText = 0;
            if (checkBox_whole_word.Checked)
            {
                indexInText = _textBox.Find(_strForFind, _startIndex, RichTextBoxFinds.WholeWord);
            }
            else
            {
                indexInText = _textBox.Find(_strForFind, _startIndex, RichTextBoxFinds.None);
            }
            if (indexInText >= 0)
            {
                _startIndex = indexInText + _strForFind.Length;
                _textBox.Focus();
            }
            else
            {
                _startIndex = 0;
                MessageBox.Show("Search reached end of document.", "Not found!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
