﻿namespace Text_Editor
{
    partial class FindForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FindForm));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_for_find = new System.Windows.Forms.TextBox();
            this.button_find_next = new System.Windows.Forms.Button();
            this.button_find_cancel = new System.Windows.Forms.Button();
            this.checkBox_whole_word = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter text: ";
            // 
            // textBox_for_find
            // 
            this.textBox_for_find.Location = new System.Drawing.Point(86, 17);
            this.textBox_for_find.Name = "textBox_for_find";
            this.textBox_for_find.Size = new System.Drawing.Size(154, 20);
            this.textBox_for_find.TabIndex = 1;
            this.textBox_for_find.TextChanged += new System.EventHandler(this.textBox_for_find_TextChanged);
            // 
            // button_find_next
            // 
            this.button_find_next.Enabled = false;
            this.button_find_next.Location = new System.Drawing.Point(259, 15);
            this.button_find_next.Name = "button_find_next";
            this.button_find_next.Size = new System.Drawing.Size(75, 23);
            this.button_find_next.TabIndex = 2;
            this.button_find_next.Text = "Find next";
            this.button_find_next.UseVisualStyleBackColor = true;
            this.button_find_next.Click += new System.EventHandler(this.button_find_next_Click);
            // 
            // button_find_cancel
            // 
            this.button_find_cancel.Location = new System.Drawing.Point(259, 56);
            this.button_find_cancel.Name = "button_find_cancel";
            this.button_find_cancel.Size = new System.Drawing.Size(75, 23);
            this.button_find_cancel.TabIndex = 3;
            this.button_find_cancel.Text = "Cancel";
            this.button_find_cancel.UseVisualStyleBackColor = true;
            this.button_find_cancel.Click += new System.EventHandler(this.button_find_cancel_Click);
            // 
            // checkBox_whole_word
            // 
            this.checkBox_whole_word.AutoSize = true;
            this.checkBox_whole_word.Location = new System.Drawing.Point(15, 60);
            this.checkBox_whole_word.Name = "checkBox_whole_word";
            this.checkBox_whole_word.Size = new System.Drawing.Size(122, 17);
            this.checkBox_whole_word.TabIndex = 4;
            this.checkBox_whole_word.Text = "Only the whole word";
            this.checkBox_whole_word.UseVisualStyleBackColor = true;
            // 
            // FindForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 92);
            this.Controls.Add(this.checkBox_whole_word);
            this.Controls.Add(this.button_find_cancel);
            this.Controls.Add(this.button_find_next);
            this.Controls.Add(this.textBox_for_find);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FindForm";
            this.Text = "Find";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_for_find;
        private System.Windows.Forms.Button button_find_next;
        private System.Windows.Forms.Button button_find_cancel;
        private System.Windows.Forms.CheckBox checkBox_whole_word;
    }
}