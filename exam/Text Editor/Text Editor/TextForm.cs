﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Text_Editor
{
    public partial class TextForm : Form
    {
        MainForm _mainForm;
        ToolStripComboBox _comboStyle;
        ToolStripComboBox _comboSize;
        ToolStripButton _buttonColor;
        ToolStripButton _buttonBackColor;
        ToolStripButton _buttonBold;
        ToolStripButton _buttonItal;
        ToolStripButton _buttonUnder;
        ToolStripButton _buttonFind;
        ToolStripMenuItem _findMenuItem;
        List<ToolStripButton> _varButtons;
        List<ToolStripMenuItem> _varMenuItems;
        bool _saved;
        public bool Saved { get { return _saved; } set { _saved = value; } }
        
        public TextForm(MainForm mainForm, ToolStripComboBox comboStyle, ToolStripComboBox comboSize, ToolStripButton buttonColor, ToolStripButton buttonBackColor, ToolStripButton buttonBold, ToolStripButton buttonItal, ToolStripButton buttonUnder, ToolStripButton buttonFind, ToolStripMenuItem findMenuItem, List<ToolStripButton> varButtons, List<ToolStripMenuItem> varMenuItems)
        {
            try
            {
                InitializeComponent();
                _mainForm = mainForm;
                _comboStyle = comboStyle;
                _comboSize = comboSize;
                _buttonColor = buttonColor;
                _buttonBackColor = buttonBackColor;
                _buttonBold = buttonBold;
                _buttonItal = buttonItal;
                _buttonUnder = buttonUnder;
                _buttonFind = buttonFind;
                _findMenuItem = findMenuItem;
                _varButtons = varButtons;
                _varMenuItems = varMenuItems;
                _saved = true;               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        private void TextArea_TextChanged(object sender, EventArgs e)
        {           
            _saved = false;           
        }

        private void TextArea_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (TextArea.Text == "")
                {
                    _buttonFind.Enabled = false;
                    _findMenuItem.Enabled = false;
                }
                else
                {
                    _buttonFind.Enabled = true;
                    _findMenuItem.Enabled = true;
                }

                if (TextArea.Text == "" || TextArea.SelectionLength == 0)
                {
                    foreach (var item in _varButtons)
                    {
                        item.Enabled = false;
                    }
                    foreach (var item in _varMenuItems)
                    {
                        item.Enabled = false;
                    }
                }
                else
                {
                    foreach (var item in _varButtons)
                    {
                        item.Enabled = true;
                    }
                    foreach (var item in _varMenuItems)
                    {
                        item.Enabled = true;
                    }
                }
                
                
                if (TextArea.SelectionFont == null)
                {
                    //_comboStyle.SelectedItem = "None";
                    //_comboSize.SelectedItem = "";
                    _buttonColor.BackColor = Color.White;
                    _buttonBackColor.BackColor = Color.White;
                }
                else
                    {
                        _comboStyle.SelectedItem = TextArea.SelectionFont.Name;
                        _comboSize.SelectedItem = (int)TextArea.SelectionFont.Size;
                        _buttonColor.BackColor = TextArea.SelectionColor;
                        _buttonBackColor.BackColor = TextArea.SelectionBackColor;
                        if (TextArea.SelectionFont.Bold == true) _buttonBold.BackColor = Color.Gray;
                        else _buttonBold.BackColor = Color.White;
                        if (TextArea.SelectionFont.Italic == true) _buttonItal.BackColor = Color.Gray;
                        else _buttonItal.BackColor = Color.White;
                        if (TextArea.SelectionFont.Underline == true) _buttonUnder.BackColor = Color.Gray;
                        else _buttonUnder.BackColor = Color.White;
                    }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void TextArea_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    TextArea.ContextMenuStrip = RichText_context_menu;
                    if (TextArea.Text == "" || TextArea.SelectionLength == 0)
                    {
                        context_copy.Enabled = false;
                        context_cut.Enabled = false;
                    }
                    if (TextArea.Text == "") 
                    {
                        context_selectAll.Enabled = false;                        
                    }
                    else
                    {
                        context_selectAll.Enabled = true;
                        if (TextArea.SelectionLength != 0)
                        {
                            context_copy.Enabled = true;
                            context_cut.Enabled = true;
                        }                       
                    }
                }     
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void context_copy_Click(object sender, EventArgs e)
        {
            _mainForm.copyToolStripMenuItem_Click(sender, e);
        }

        private void context_paste_Click(object sender, EventArgs e)
        {
            _mainForm.pasteToolStripMenuItem_Click(sender, e);
        }

        private void context_cut_Click(object sender, EventArgs e)
        {
            _mainForm.cutToolStripMenuItem_Click(sender, e);
        }

        private void context_selectAll_Click(object sender, EventArgs e)
        {
            _mainForm.selectAllToolStripMenuItem_Click(sender, e);
        }       
    }
}
