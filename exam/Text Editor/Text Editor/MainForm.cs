﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Printing;

namespace Text_Editor   
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            try
            {
                InitializeComponent(); 
                DesablingMenuItems();
                InitControls();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        void InitControls()
        {
            try
            {
                InstalledFontCollection fontList = new InstalledFontCollection();
                foreach (FontFamily ff in fontList.Families)
                {
                    toolStripComboBox_font_type.Items.Add(ff.Name);
                }
                int[] fontSizeArr = { 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72 };
                foreach (int i in fontSizeArr)
                {
                    toolStripComboBox_font_size.Items.Add(i);
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                List<ToolStripButton> varButtons = InitButtonList();
                List<ToolStripMenuItem> varMenuItems = InitMenuList();                
                Form ChildForm = new TextForm(this, toolStripComboBox_font_type, toolStripComboBox_font_size, toolStripButton_font_color, toolStripButton_back_color, toolStripButton_bold, toolStripButton_ital, toolStripButton_underline, toolStripButton_find, findToolStripMenuItem, varButtons, varMenuItems); 
                ChildForm.MdiParent = this;
                ChildForm.Dock = DockStyle.Fill;
                ChildForm.Text = " new document " + this.MdiChildren.Length.ToString();
                ChildForm.Show();
                EnablingToolBar();
                foreach (var item in varButtons) 
                {
                    item.Enabled = false;
                }
                foreach (var item in varMenuItems)
                {
                    item.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        List<ToolStripButton> InitButtonList()
        {
            List<ToolStripButton> buttons = new List<ToolStripButton>();
            buttons.Add(copyToolStripButton);
            buttons.Add(cutToolStripButton);
            return buttons;
        }

        List<ToolStripMenuItem> InitMenuList()
        {
            List<ToolStripMenuItem> menuItems = new List<ToolStripMenuItem>();
            menuItems.Add(copyToolStripMenuItem);
            menuItems.Add(cutToolStripMenuItem);
            menuItems.Add(selectAllToolStripMenuItem);
            return menuItems;
        }

        void EnablingToolBar()
        {
            try
            {
                if (this.MdiChildren.Length > 0)
                {
                    foreach (ToolStripItem item in tool_strip.Items)
                    {
                        item.Enabled = true;
                    }
                }
                toolStripComboBox_font_type.SelectedItem = "Times New Roman";
                toolStripComboBox_font_size.SelectedItem = 12;
                toolStripButton_font_color.BackColor = Color.Black;
                toolStripButton_back_color.BackColor = Color.White;
                EnablingMenuItems();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        void DesablingToolBar()
        {
            try
            {
                if (this.MdiChildren.Length == 0)
                {
                    for (int i = 2; i < tool_strip.Items.Count - 1; ++i)
                    {
                        tool_strip.Items[i].Enabled = false;
                    }
                }
                DesablingMenuItems();
                toolStripButton_font_color.BackColor = Color.Black;
                toolStripButton_back_color.BackColor = Color.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        void DesablingMenuItems()
        {
            try
            {
                editToolStripMenuItem.Enabled = false;
                formatToolStripMenuItem.Enabled = false;
                windowsToolStripMenuItem.Enabled = false;
                saveToolStripMenuItem.Enabled = false;
                saveAsToolStripMenuItem.Enabled = false;
                printToolStripMenuItem.Enabled = false;
                closeToolStripMenuItem.Enabled = false;
                closeAllToolStripMenuItem.Enabled = false;
                layoutToolStripMenuItem.Enabled = false;
                toolStripButton_find.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }

        void EnablingMenuItems()
        {
            try
            {
                editToolStripMenuItem.Enabled = true;
                formatToolStripMenuItem.Enabled = true;
                windowsToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;
                saveAsToolStripMenuItem.Enabled = true;
                printToolStripMenuItem.Enabled = true;
                closeToolStripMenuItem.Enabled = true;
                closeAllToolStripMenuItem.Enabled = true;
                layoutToolStripMenuItem.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }    
        }
        
        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void helpToolStripButton_Click(object sender, EventArgs e)
        {
            AboutForm about = new AboutForm();
            about.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {                
                TextForm curForm = GetCurrentTextForm();
                if (curForm.Saved == false)
                {
                    SaveForm saveForm = new SaveForm(this, curForm);
                    saveForm.ShowDialog();
                }
                else curForm.Close();
                if (this.MdiChildren.Length == 0)   DesablingToolBar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Form[] controlCol = this.MdiChildren;
                foreach (TextForm item in controlCol)
                {
                    if (item.Saved == false)
                    {
                        SaveForm saveForm = new SaveForm(this, item);
                        saveForm.ShowDialog();
                    }
                    else item.Close(); 
                }
                DesablingToolBar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void toolStripButton_close_Click(object sender, EventArgs e)
        {
            closeToolStripMenuItem_Click(sender, e);
        }

        private void newToolStripButton_Click(object sender, EventArgs e)
        {
            newToolStripMenuItem_Click(sender, e);
        }

        private void openToolStripButton_Click(object sender, EventArgs e)
        {
            openToolStripMenuItem_Click(sender, e);
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFD = new OpenFileDialog())
            {
                openFD.Filter = "Rtf files(*.rtf)|*.rtf|Text files(*.txt)|*.txt|All files(*.*)|*.*";
                openFD.FilterIndex = 3;
                openFD.CheckFileExists = true;
                openFD.InitialDirectory = @"D:\";
                if (openFD.ShowDialog() == DialogResult.OK)
                {
                    newToolStripMenuItem_Click(sender, e);                  
                    RichTextBox currentDoc = GetActivText();
                    if (currentDoc != null)
                    {
                        try
                        {
                            if (Path.GetExtension(openFD.FileName).ToLower() == @".rtf")   // если файл в формате RTF
                            {
                                currentDoc.Parent.Text = openFD.FileName;
                                currentDoc.LoadFile(openFD.FileName);
                            }
                            else
                            {
                                using (StreamReader sReader = new StreamReader(openFD.FileName, Encoding.Default))
                                {
                                    currentDoc.Parent.Text = openFD.FileName;
                                    currentDoc.Text = sReader.ReadToEnd();
                                    sReader.Dispose();
                                }
                            }
                            toolStripComboBox_font_type.SelectedItem = currentDoc.SelectionFont.Name;
                            toolStripComboBox_font_size.SelectedItem = (int)currentDoc.SelectionFont.Size;
                            toolStripButton_font_color.BackColor = currentDoc.SelectionColor;
                            toolStripButton_back_color.BackColor = currentDoc.SelectionBackColor;
                            TextForm curForm = GetCurrentTextForm();
                            curForm.Saved = true;
                        }
                        catch (ArgumentException)   
                        {
                            MessageBox.Show("Error file`s type!");
                        }
                        catch (IOException)
                        {
                            MessageBox.Show("Error openning file!");
                        }                            
                    }
                    else
                    {
                        MessageBox.Show("Any text area is active!");    
                    }                    
                }
                openFD.Dispose();
            }
        }

        RichTextBox GetActivText()
        { 
            ControlCollection controlCol = (ControlCollection)this.ActiveMdiChild.Controls;
            if (controlCol[0] as RichTextBox != null)
            {
                RichTextBox currentDoc = controlCol[0] as RichTextBox;
                return currentDoc;
            }
            else return null;                      
        }

        TextForm GetCurrentTextForm()
        {
            ControlCollection controlCol = (ControlCollection)this.ActiveMdiChild.Controls;
            RichTextBox currentDoc = controlCol[0] as RichTextBox;
            if (currentDoc.Parent as TextForm != null) return currentDoc.Parent as TextForm;
            else return null;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                Regex reg = new Regex(@"^[A-Z]:\\");
                if (!reg.IsMatch(currentDoc.Parent.Text))
                {
                    saveAsToolStripMenuItem_Click(sender, e);
                    return;
                }
                if (currentDoc != null)
                {
                    try
                    {
                        if (Path.GetExtension(currentDoc.Parent.Text).ToLower() == ".rtf")   // если файл в формате RTF
                        {
                            currentDoc.SaveFile(currentDoc.Parent.Text);
                            TextForm curForm = GetCurrentTextForm();
                            curForm.Saved = true;
                        }
                        else
                        {
                            using (StreamWriter sWriter = new StreamWriter(currentDoc.Parent.Text))
                            {
                                sWriter.Write(currentDoc.Text);
                                TextForm curForm = GetCurrentTextForm();
                                curForm.Saved = true;
                                sWriter.Dispose();
                            }
                        }
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Error saving file!");
                    }
                    catch (ArgumentException)
                    {
                        MessageBox.Show("Error file`s type!");
                    }
                }
                else
                {
                    MessageBox.Show("Any one text area is not active!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void saveToolStripButton_Click(object sender, EventArgs e)
        {
            saveToolStripMenuItem_Click(sender, e);
        }

        internal void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFD = new SaveFileDialog())
            {
                RichTextBox currentDoc = GetActivText();
                saveFD.Filter = "Rtf files(*.rtf)|*.rtf|Text files(*.txt)|*.txt|All files(*.*)|*.*";
                saveFD.FilterIndex = 1;
                saveFD.CheckFileExists = true;
                saveFD.InitialDirectory = @"D:\";
                saveFD.CheckFileExists = false;
                saveFD.FileName = Path.GetFileName(currentDoc.Parent.Text);
                saveFD.DefaultExt = ".rtf";
                if (saveFD.ShowDialog() == DialogResult.OK)
                {                   
                    if (currentDoc != null)
                    {
                        try
                        {
                            if (Path.GetExtension(currentDoc.Parent.Text).ToLower() == ".rtf")   // если файл в формате RTF
                            {
                                if (!File.Exists(saveFD.FileName))
                                {
                                    using (File.Create(saveFD.FileName)) { }
                                    //currentDoc.SaveFile(saveFD.FileName, RichTextBoxStreamType.RichText);                                    
                                }                               
                                currentDoc.SaveFile(saveFD.FileName);                                
                            }
                            else
                            {
                                if (!File.Exists(saveFD.FileName))
                                {
                                    using (File.Create(saveFD.FileName)) { }
                                    currentDoc.SaveFile(saveFD.FileName, RichTextBoxStreamType.PlainText);                                    
                                }
                            }
                            currentDoc.Parent.Text = saveFD.FileName;
                            TextForm curForm = GetCurrentTextForm();
                            curForm.Saved = true;
                        }
                        catch (IOException)
                        {
                            MessageBox.Show("Error saving file!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Any text area is active!");
                    }
                }                
                saveFD.Dispose();
            }
        }

        private void printToolStripButton_Click(object sender, EventArgs e)
        {
           printToolStripMenuItem_Click(sender, e);
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                PrintDocument PrintDoc = new PrintDocument();
                PrintDoc.DocumentName = currentDoc.Parent.Text;
                PrintDialog printDial = new PrintDialog();
                printDial.AllowSomePages = true;
                printDial.Document = PrintDoc;
                if (printDial.ShowDialog() == DialogResult.OK)
                {
                    PrintDoc.Print();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeAllToolStripMenuItem_Click(sender, e);
            this.Close();
        }       

        private void fontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                fontDial.ShowColor = true;
                fontDial.Font = currentDoc.SelectionFont;
                fontDial.Color = currentDoc.SelectionColor;
                if (fontDial.ShowDialog() == DialogResult.OK)
                {
                    currentDoc.SelectionFont = fontDial.Font;
                    currentDoc.SelectionColor = fontDial.Color;
                    int i = (int)currentDoc.SelectionFont.Size;                    
                    string str = i.ToString();
                    toolStripComboBox_font_size.Text = str;
                    str = currentDoc.SelectionFont.Name;
                    toolStripComboBox_font_type.Text = str;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void colorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                colorDial.Color = currentDoc.SelectionColor;
                if (colorDial.ShowDialog() == DialogResult.OK)
                {
                    currentDoc.SelectionColor = colorDial.Color;
                    toolStripButton_font_color.BackColor = colorDial.Color;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private void fontBacColorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                colorDial.Color = currentDoc.SelectionBackColor;
                if (colorDial.ShowDialog() == DialogResult.OK)
                {
                    currentDoc.SelectionBackColor = colorDial.Color;
                    toolStripButton_back_color.BackColor = colorDial.Color;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }     

        private void toolStripButton_bold_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.SelectionFont != null)
                {
                    System.Drawing.Font currentFont = currentDoc.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;
                    if (currentDoc.SelectionFont.Bold == true)
                    {
                        toolStripButton_bold.BackColor = Color.White;
                        if (currentDoc.SelectionFont.Italic == true && currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Underline | FontStyle.Italic;
                        else
                            if (currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Italic;
                            else
                                if (currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Underline;
                                else newFontStyle = FontStyle.Regular;
                    }
                    else
                    {
                        toolStripButton_bold.BackColor = Color.Gray;
                        if (currentDoc.SelectionFont.Italic == true && currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Italic | FontStyle.Bold | FontStyle.Underline;
                        else
                            if (currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Bold | FontStyle.Italic;
                            else
                                if (currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Bold | FontStyle.Underline;
                                else newFontStyle = FontStyle.Bold;
                    }
                    currentDoc.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void toolStripButton_ital_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.SelectionFont != null)
                {
                    System.Drawing.Font currentFont = currentDoc.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;
                    if (currentDoc.SelectionFont.Italic == true)
                    {
                        toolStripButton_ital.BackColor = Color.White;
                        if (currentDoc.SelectionFont.Bold == true && currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Underline | FontStyle.Bold;
                        else
                            if (currentDoc.SelectionFont.Bold == true) newFontStyle = FontStyle.Bold;
                            else
                                if (currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Underline;
                                else newFontStyle = FontStyle.Regular;
                    }
                    else
                    {
                        toolStripButton_ital.BackColor = Color.Gray;
                        if (currentDoc.SelectionFont.Bold == true && currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Italic | FontStyle.Bold | FontStyle.Underline;
                        else
                            if (currentDoc.SelectionFont.Bold == true) newFontStyle = FontStyle.Bold | FontStyle.Italic;
                            else
                                if (currentDoc.SelectionFont.Underline == true) newFontStyle = FontStyle.Italic | FontStyle.Underline;
                                else  newFontStyle = FontStyle.Italic;
                    }
                    currentDoc.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        private void toolStripButton_underline_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.SelectionFont != null)
                {
                    System.Drawing.Font currentFont = currentDoc.SelectionFont;
                    System.Drawing.FontStyle newFontStyle;
                    if (currentDoc.SelectionFont.Underline == true)
                    {
                        toolStripButton_underline.BackColor = Color.White;
                        if (currentDoc.SelectionFont.Bold == true && currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Italic | FontStyle.Bold;
                        else
                            if (currentDoc.SelectionFont.Bold == true) newFontStyle = FontStyle.Bold;
                            else
                                if(currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Italic;
                                else newFontStyle = FontStyle.Regular;
                    }
                    else
                    {
                        toolStripButton_underline.BackColor = Color.Gray;
                        if (currentDoc.SelectionFont.Bold == true && currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Italic | FontStyle.Bold | FontStyle.Underline;
                        else
                            if (currentDoc.SelectionFont.Bold == true) newFontStyle = FontStyle.Bold | FontStyle.Underline;
                            else
                                if (currentDoc.SelectionFont.Italic == true) newFontStyle = FontStyle.Italic | FontStyle.Underline;
                                else newFontStyle = FontStyle.Underline;
                    }
                    currentDoc.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } 
        }

        bool exit = true;
        internal void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                Form[] controlCol = this.MdiChildren;
                foreach (TextForm item in controlCol)
                {
                    if (item.Saved == false)
                    {
                        SaveForm saveForm = new SaveForm(this, item);
                        saveForm.ShowDialog();
                        if (saveForm.Canceled)
                        {
                            e.Cancel = true;
                        }
                    }
                    else item.Close();
                }
                DesablingToolBar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        internal void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox currentDoc = GetActivText();
            currentDoc.SelectAll();
        }

        internal void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.Clear();
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.SelectionLength > 0)
                {
                    currentDoc.Copy();
                }
                //copy image in clipboard
                if ((currentDoc.SelectionType & RichTextBoxSelectionTypes.Object) == RichTextBoxSelectionTypes.Object)
                {
                    currentDoc.Copy();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }      
            
        }

        internal void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (Clipboard.GetDataObject().GetDataPresent(DataFormats.Text) == true)
                {
                    currentDoc.Paste();
                }
                DataFormats.Format format = DataFormats.GetFormat(DataFormats.Bitmap);
                if (currentDoc.CanPaste(format))
                {
                    currentDoc.Paste(format);
                }               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }               
        }

        internal void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.SelectedText != "")
                {
                    currentDoc.Cut();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }                
        }

        private void cutToolStripButton_Click(object sender, EventArgs e)
        {
            cutToolStripMenuItem_Click(sender, e);
        }

        private void copyToolStripButton_Click(object sender, EventArgs e)
        {
            copyToolStripMenuItem_Click(sender, e);
        }

        private void pasteToolStripButton_Click(object sender, EventArgs e)
        {
            pasteToolStripMenuItem_Click(sender, e);
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.CanUndo == true)
                {
                    currentDoc.Undo();
                    // currentDoc.ClearUndo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }      

        private void toolStripButton_undo_Click(object sender, EventArgs e)
        {
            undoToolStripMenuItem_Click(sender, e);
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                if (currentDoc.CanRedo == true)
                {
                    currentDoc.Redo();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }
        
        private void toolStripButton_redo_Click(object sender, EventArgs e)
        {
           redoToolStripMenuItem_Click(sender, e);
        }

        private void toolStripButton_align_left_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                currentDoc.SelectionAlignment = HorizontalAlignment.Left;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }
        
        private void toolStripButton_align_center_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                currentDoc.SelectionAlignment = HorizontalAlignment.Center;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }

        private void toolStripButton_align_right_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                currentDoc.SelectionAlignment = HorizontalAlignment.Right;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }

        private void toolStripComboBox_font_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                int size = 0;
                int.TryParse(toolStripComboBox_font_size.Text, out size);
                if(size !=0 ) currentDoc.SelectionFont =  new Font(toolStripComboBox_font_type.Text, size);
                currentDoc.Focus();    
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }

        private void toolStripComboBox_font_size_SelectedIndexChanged(object sender, EventArgs e)
        {
            toolStripComboBox_font_type_SelectedIndexChanged(sender, e);
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            helpToolStripButton_Click(sender, e);
        }

        private void toolStripButton_font_color_Click(object sender, EventArgs e)
        {
            colorToolStripMenuItem_Click(sender, e);
        }

        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                RichTextBox currentDoc = GetActivText();
                FindForm fForm = new FindForm(currentDoc);
                fForm.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }        
        }

        private void toolStripButton_find_Click(object sender, EventArgs e)
        {
            findToolStripMenuItem_Click(sender, e);
        }

        private void bulletToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox currentDoc = GetActivText();
            currentDoc.SelectionBullet = true;
        }

        private void toolStripButton_bullet_Click(object sender, EventArgs e)
        {
            bulletToolStripMenuItem_Click(sender, e);
        }

        private void useIndentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RichTextBox currentDoc = GetActivText();
            currentDoc.SelectionIndent = 20;
        }
    }
}
