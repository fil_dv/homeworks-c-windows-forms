﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Text_Editor
{
    public partial class SaveForm : Form
    {
        MainForm _mainForm;
        TextForm _textForm;
        bool _canceled;
        public bool Canceled { get { return _canceled; } }
        
        public SaveForm(MainForm mainForm, TextForm textForm)
        {
            this.StartPosition = FormStartPosition.CenterParent;
            InitializeComponent();
            _mainForm = mainForm;
            _textForm = textForm;
            _canceled = false;
            this.Text = textForm.Text;
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            _mainForm.saveAsToolStripMenuItem_Click(sender, e);
            button_cancel_Click(sender, e);
            _canceled = false;
        }

        private void button_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
            _canceled = true;
            //_mainForm.MainForm_FormClosing(sender, new FormClosingEventArgs(CloseReason.UserClosing, true));
        }

        private void button_dont_save_Click(object sender, EventArgs e)
        {
            _textForm.Close();
            button_cancel_Click(sender, e);
            _canceled = false;
        }
    }
}
