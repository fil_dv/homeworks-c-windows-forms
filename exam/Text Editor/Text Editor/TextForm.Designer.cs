﻿namespace Text_Editor
{
    partial class TextForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TextForm));
            this.TextArea = new System.Windows.Forms.RichTextBox();
            this.RichText_context_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.context_copy = new System.Windows.Forms.ToolStripMenuItem();
            this.context_paste = new System.Windows.Forms.ToolStripMenuItem();
            this.context_cut = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.context_selectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.RichText_context_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextArea
            // 
            this.TextArea.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TextArea.Location = new System.Drawing.Point(0, 0);
            this.TextArea.Name = "TextArea";
            this.TextArea.Size = new System.Drawing.Size(284, 262);
            this.TextArea.TabIndex = 0;
            this.TextArea.Text = "";
            this.TextArea.SelectionChanged += new System.EventHandler(this.TextArea_SelectionChanged);
            this.TextArea.TextChanged += new System.EventHandler(this.TextArea_TextChanged);
            this.TextArea.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextArea_MouseDown);
            // 
            // RichText_context_menu
            // 
            this.RichText_context_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.context_copy,
            this.context_paste,
            this.context_cut,
            this.toolStripSeparator1,
            this.context_selectAll});
            this.RichText_context_menu.Name = "RichText_context_menu";
            this.RichText_context_menu.Size = new System.Drawing.Size(121, 98);
            // 
            // context_copy
            // 
            this.context_copy.Image = ((System.Drawing.Image)(resources.GetObject("context_copy.Image")));
            this.context_copy.Name = "context_copy";
            this.context_copy.Size = new System.Drawing.Size(120, 22);
            this.context_copy.Text = "Copy";
            this.context_copy.Click += new System.EventHandler(this.context_copy_Click);
            // 
            // context_paste
            // 
            this.context_paste.Image = ((System.Drawing.Image)(resources.GetObject("context_paste.Image")));
            this.context_paste.Name = "context_paste";
            this.context_paste.Size = new System.Drawing.Size(120, 22);
            this.context_paste.Text = "Paste";
            this.context_paste.Click += new System.EventHandler(this.context_paste_Click);
            // 
            // context_cut
            // 
            this.context_cut.Image = ((System.Drawing.Image)(resources.GetObject("context_cut.Image")));
            this.context_cut.Name = "context_cut";
            this.context_cut.Size = new System.Drawing.Size(120, 22);
            this.context_cut.Text = "Cut";
            this.context_cut.Click += new System.EventHandler(this.context_cut_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(117, 6);
            // 
            // context_selectAll
            // 
            this.context_selectAll.Name = "context_selectAll";
            this.context_selectAll.Size = new System.Drawing.Size(120, 22);
            this.context_selectAll.Text = "Select all";
            this.context_selectAll.Click += new System.EventHandler(this.context_selectAll_Click);
            // 
            // TextForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.TextArea);
            this.Name = "TextForm";
            this.Text = "TextField";
            this.RichText_context_menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox TextArea;
        private System.Windows.Forms.ContextMenuStrip RichText_context_menu;
        private System.Windows.Forms.ToolStripMenuItem context_copy;
        private System.Windows.Forms.ToolStripMenuItem context_paste;
        private System.Windows.Forms.ToolStripMenuItem context_cut;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem context_selectAll;
    }
}